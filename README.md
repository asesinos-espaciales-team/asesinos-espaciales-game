# Asesinos Espaciales

pew! pew! pew!

## Preparando el entorno de trabajo

Los requisitos del entorno de desarrollo son los siguientes:
 *  IDE Eclipse Oxygen 3a
 *  Java JDK 1.8
 *  git
 *  Conexión a internet  
 
 Lo primero que debemos hacer es clonar el proyecto:
 git clone <URL del proyecto> <carpeta-destino opcional>  
 
 Consulte en el repositorio del proyecto las URL para copia, se recomienda usar
 SSH en lugar de HTTPS  

El siguiente paso consiste en abrir eclipse e ir a la barra de menúes y elegir
file > Open Projects from File System...  

Elegimos la carpeta donde hayamos clonado el proyecto y Eclipse debe reconocer
el proyecto para abrirlo.  

Ahora no queda más que esperar a que se terminen de carcar las dependencias
del proyecto.

## Ejecutando el proyecto

Para ejecutar al proyecto, basta con ejecutar el main en la clase
*mx.asesinosespaciales.ae.AsesinosEspaciales* del WAR generado por Eclipse
al terminar de compilar; o desde las herramientas para ejecutar un archivo en
el mismo IDE.

## Generando la documentación técnica

Una vez que hemos abierto el proyecto en Eclipse, nos dirigimos a la barra de
menús y elegimos project > generate Javadoc....  

Configuramos el exportador, usando el directorio doc en la raíz del proyecto
como el destino de la documentación. 

## Reportando un problema

Para reportar un problema, use los *issues* del repositorio con la siguiente
información:
-  Título: descripción corta del problema
-  Descripción: Cómo ocurrió el problema, de preferencia cómo replicarlo

