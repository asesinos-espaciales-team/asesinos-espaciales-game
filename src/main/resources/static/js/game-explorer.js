var partidaSolicitada = -1;

function unirse(partidaId) {
	let eventoWS = buildEventoWS(partidaId, 0, $("#tokenSesion").val(),
			$('#username').val()) 
	partidaSolicitada = partidaId;
	sendMessage("/lobbyAction", eventoWS)
}

function verPartida(partidaId) {
	window.location.href = '/jugar?idPartida=' +partidaId
}

function abandonarPartida(idPartida, yaExpulsado = false) {
	$.each($('button[name="unirseP"]'), function() {
		if(this == $('#unirseP-' +idPartida).get(0)) {
			$(this).text('Unirse a la partida')
			$(this).unbind('click')
			$(this).removeAttr('onclick')
			$(this).click(function() {
				unirse(idPartida)
			})
		} else {
			$(this).prop('disabled', false)
		}
	})
	$('#equipos').hide()
	$('#mi-equipo').hide()
	$('#equipos-partida-' +idPartida).hide()
	$('#mi-equipo-partida-' +idPartida).hide()
	if(!yaExpulsado) {
		let eventoWS = buildEventoWS(idPartida, 1, $('#tokenSesion').val(),
				$('#username').val())
		sendMessage("/lobbyAction", eventoWS)
	}
}

function cambiaEquipo(idPartida) {
	let selectorEquipo = $('#mi-equipo-partida-' +idPartida).val()
	let eventoLobby = {
			tokenSesion : $('#tokenSesion').val(),
			tipoEvento : 6,
			datosAdicionales : $('#username').val(),
			roomid : idPartida,
			equipos : [selectorEquipo ? selectorEquipo : 1]
	}
	sendMessage("/lobbyAction", JSON.stringify(eventoLobby))
}

function actualizaEquipos(idPartida, equipos) {
	equipos = parseInt(equipos)
	$.each($('button[name="unirseP"]'), function() {
		if(this == $('#unirseP-' +idPartida).get(0)) {
			$(this).text('Abandonar partida')
			$(this).unbind('click')
			$(this).removeAttr('onclick')
			$(this).click(function() {
				abandonarPartida(idPartida)
			})
		} else {
			//$(this).prop('disabled', true)
			$(this).prop('disabled', false)
		}
	})
	$('#equipos').show()
	$('#mi-equipo').show()
	$('#equipos-partida-' +idPartida).text(equipos +1)
	$('#equipos-partida-' +idPartida).show()
	let selectEquipo = $('#mi-equipo-partida-' +idPartida)
	selectEquipo.empty()
	selectEquipo.change(function() {
		cambiaEquipo(idPartida)
	})
	for(let i = 0; i <= equipos +1; i++) {
		selectEquipo.append(
				$('<option>', {'val' : i})
				.text(i))
	}
	selectEquipo.val("1").change()
	selectEquipo.show()
}

function init() {
	connect(function(event) {
		let eventoLobby = JSON.parse(event.body)
		switch(eventoLobby.tipoEvento) {
		case 0 : // usuario conectado
			if($('#username').val() == eventoLobby.datosAdicionales &&
					eventoLobby.roomid == partidaSolicitada) {
				actualizaEquipos(eventoLobby.roomid, eventoLobby.equipos)
			}
			break
		case 1 : // usuario desconectado
			if($('#username').val() == eventoLobby.datosAdicionales &&
					eventoLobby.roomid == partidaSolicitada)
				alert("Has sido expulsado de la partida")
				location.reload()
			break
		case 2 : // usuario listo
			console.log("Usuario listo " +eventoLobby.datosAdicionales)
			break
		case 3 : // usuario no listo
			console.log("Usuario no listo " +eventoLobby.datosAdicionales)
			break
		case 4 : // iniciar partida
			if(eventoLobby.roomid == $('#partida-en-curso').val())
				window.location.href = '/jugar?idPartida=' +eventoLobby.roomid
			break
		case 5 : // borrar partida
			if(eventoLobby.roomid == partidaSolicitada) {
				alert("La partida ha sido cancelada")
				abandonarPartida(eventoLobby.roomid, true)
			} else {
				location.reload()
			}
			break
		default :
			console.log("Tipo de evento de lobby desconocido: " +eventoLobby)
			return
		}
	}, '/evento/lobby', function() {
		if($('#partida-en-curso').length) {
			actualizaEquipos($('#partida-en-curso').val(), $('#equipo-partida').val())			
		}
	})
}

function nextPage(curPag) {
	window.location.href = '/verPartida?page=' +(curPag +1)
}

function previousPage(curPag) {
	window.location.href = '/verPartida?page=' +(curPag -1)
}

$(window).on("unload", disconnect)

$(document).ready(init)
