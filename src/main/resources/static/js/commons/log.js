/*
 * Script que se encarga de manejar los formularios de seguimiento de sesión de
 * usaurio. 
 */

/*
 * Cambia la pestaña que se muestra en el panel de inicio de sesión y registro
 * de usuario.
 * param evt - El evento de click sobre la pestaña seleccionada.
 * param tabName - el identificador de la pestaña seleccionada.
 * returns undefined
 */
function openTab(evt, tabName) {
	var tabcontent = document.getElementsByClassName("tabcontent");
	for(let i = 0; i < tabcontent.length; i++) {
		tabcontent[i].style.display = "none"
	}
	var tablinks = document.getElementsByClassName("tablinks");
	for(let i = 0; i < tablinks.length; i++) {
		tablinks[i].className = tablinks[i].className.replace(" active", "");
	}
	document.getElementById(tabName).style.display = "block"
	evt.currentTarget.className += " active"
}//openTab

// Muestra el popup con los formularios para iniciar sesión y registro de usuario
function displayLogForms() {
	document.getElementById("log-popup-forms").style.display = "block"
}//displayLogForms

// Cierra el popup con los formularios para iniciar sesión y registro de usaurio
function closeLogForm() {
	document.getElementById("log-popup-forms").style.display = "none"
}//closeLogForm

/*
 * Envía los datos del formulario al servidor.
 * param form - El formulario a enviar.
 * param action_url - La URL del servidor que recibe la información del formulario
 * returns undefined
 */
function sessionSubmit(form, action_url) {
	$.ajax({
		type: 'POST',
		url: action_url,
		data: $(form).serialize(),
		success: function() {
			location.reload()
		},
		error: function(request, status, error) {
			if(request.status == 404) alert("El usuario no existe. Tal vez quieras registrate")
			else if(request.status == 401) alert("Acceso incorrecto")
			else if(request.status == 406) alert("- Debes proporcionar un "
					+"corre electrónico válido.\n- El nombre de usuario no "
					+"puede tener más de 128 caracteres")
			else if(request.status == 409) alert("Correo o usuario ya registrado")
			else alert("Error. Por favor, intentelo de nuevo más tarde")
		}	
	})// AJAX arranca grasa
}// sessionSubmit

// Termina la sesión del usuario
function logout() {
	$.ajax({
		type: 'POST',
		url: '/logout',
		success: function() {
			location.reload()
		},
		error: function(request, status, error) {
			alert("El token de sesion no es válido")
		}
	})// ¡hay AJAX!!!
}//logout