var stompClient = null

/*
 * Establece una conexión por web sockets.
 * param managed - true indica que el web socket es administrado por este
 * script. false indica que el web socket es administrado por quien invoque
 * esta función.
 * returns STOMPClient - Si managed es false, se devuelve el web socket creado.
 * En otro caso devuelve undefined. 
 */
function connect(listener, listenEndpoint, ready = null, managed = true) {
	if(managed) {
		var socket = new SockJS('/aews')
		stompClient = Stomp.over(socket)
		stompClient.connect({}, function(frame) {
			stompClient.subscribe('/ascomm' +listenEndpoint, listener)
		})
		if(ready) ready()
		return undefined;
	} else {
		let skt = new SockJS('/awes')
		let client = Stomp.over(skt)
		client.connect({}, function(frame) {
			client.subscribe('/ascomm' +listenEndpoint, listener)
		})
		if(ready) ready()
		return {
			socket : skt,
			stompClient : client
		} 
	}
}

function connectWrapper(listener, listenEndpoint, ready = null, managed = true) {
	
	return function() {
		connect(listener, listenEndpoint, ready, managed);
	} 
	
} 

function disconnect(client = stompClient) {
	if(client !== null) {
		client.disconnnect()
	}
	localStorage.clear()
}

function disconnectWrapper(client = stompClient) {
	
	return function() {
		disconnect(client)
	}
	
}

function sendMessage(endpoint, eventoWS, client = stompClient) {
	if(!client.connected) return
	client.send("/app" +endpoint, {}, eventoWS)
}

function buildEventoWS(partidaId, tipo, token, datos = null) {
	return JSON.stringify({
		tipoEvento : tipo,
		roomid : partidaId,
		tokenSesion : token,
		datosAdicionales : datos
	})
}