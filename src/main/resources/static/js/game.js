var posAbsEstrellas
var dimensionEstrella = {
		x : 3,
		y : 3
}

var campoEstrellas = $('#campo-estrellas')
var espacioProfundo = $('#espacio-profundo')
var posJugador = {
		x : 1,
		y : 0
}
var posicionCamara = {
		x : 0,
		y : 0
}

var username = $('#username').val()
var margen = {
		x : espacioProfundo.width() /2,
		y : espacioProfundo.height() /2
}

var busy = false;
var tokenSesion = $('#tokenSesion').val()
var idPartida = $('#idPartida').val()

function creaCampoDeEstrellas() {
	let posEstrellas = []
	let ancho = $('#ancho-espacioprof').val()
	let alto = $('#largo-espacioprof').val()
	campoEstrellas.width(ancho)
	campoEstrellas.height(alto)
	for(let i = 0; i < alto; i++) {
		for(let j = 0; j < ancho; j++) {
			if(Math.random() >= 0.999) {
				posEstrellas.push({x:j,y:i})
				i += 10
				break
			}
		}
	}
	return posEstrellas
}//creaCampoDeEstrellas

/*
 * Indica si las coordenadas dadas están dentro del cuadro de la cámara del
 * jugador
 * param pos - La posición del objeto que se quiere saber está o no en cuadro
 * param dimension - Las dimensiones del mismo objeto
 * retirns boolean
 */ 
function enCuadro(pos, dimension) {
	return (pos.x < posicionCamara.x +espacioProfundo.width() &&
			posicionCamara.x < pos.x +dimension.x &&
			pos.y < posicionCamara.y +espacioProfundo.height() &&
					posicionCamara.y < pos.y +dimension.y)
}//enCuadro

function calcularPosicionRelativa(pos) {
	let posRelativa = {
			x : pos.x -posicionCamara.x,
			y : pos.y -posicionCamara.y
	}
	if(posicionCamara.x < 0 && campoEstrellas.width() +posicionCamara.x < pos.x) {
		posRelativa.x = pos.x -(campoEstrellas.width() +posicionCamapra.x)
	} else if(posicionCamara.x +espacioProfundo.width() > campoEstrellas.width()
			&& pos.x < posicionCamara.x +espacioProfundo.width() -campoEstrellas.width()) {
		posRelativa.x = campoEstrellas.width() -(posicionCamapra.x +margin.x) +pos.x
	}
	if(posicionCamara.y < 0 && campoEstrellas.height() +posicionCamara.y < pos.y) {
		posRelativa.y = pos.y -(campoEstrellas.height() +posicionCamara.y)
	} else if(posicionCamara.y +espacioProfundo.height() > campoEstrellas.height()
			&& pos.y < posicionCamara.y +espacioProfundo.height() -campoEstrellas.height()) {
		posRelativa.y = campoEstrellas.height() -(posicionCamapra.y +margin.y) +pos.y
	}
	return posRelativa
}

function dibujarEspacio() {
	espacioProfundo.empty()
	campoEstrellas.empty()
	for(let i = 0; i < posAbsEstrellas.length; i++) {
		let pos = posAbsEstrellas[i];
		let posRelativa = calcularPosicionRelativa(pos)
		if(enCuadro(posRelativa, dimensionEstrella)) {
			campoEstrellas.append('<div class="estrella" style="left:'
					+posRelativa.x +'px;top:'
					+posRelativa.y +'px" />')
		}
	}
}//dibujarEspacio

function calcularPosicionCamara(desp) {
	posicionCamara.x += desp[0]
	posicionCamara.y += desp[1]
	if(posicionCamara.x < -margen.x) {
		posicionCamara.x = espacioProfundo.width() -margen.x
	} else if(posicionCamara.x > espacioProfundo.width() -margen.x) {
		posicionCamara.x = -margen.x
	}
	if(posicionCamara.y < -margen.y) {
		posicionCamara.y = espacioProfundo.height() -margen.y
	} else if(posicionCamara.y > espacioProfundo.height() -margen.y) {
		posicionCamara.y = -margen.y
	}
}//calcularPosicionCamara

function dibujarAsteroides(asteroides, dimensiones) {
	for(let i = 0; i < asteroides.length; i++) {
		let pos = {
				x : asteroides[i][0],
				y : asteroides[i][1]
		}
		let posRelativa = calcularPosicionRelativa(pos)
		let dimens = {
			x : dimensiones[0],
			y : dimensiones[1]
		}
		if(enCuadro(posRelativa, dimens)) {
			espacioProfundo.append('<div class="sprite" style="height:'
					+dimens.x +'px;width:' +dimens.y +'px;left:'
					+posRelativa.x +'px;top:' +posRelativa.y +'px" />')
		}
	}
}

function dibujarDisparos(disparos, dimensiones) {
	for(let i = 0; i < disparos.length; i++) {
		let pos = {
				x : disparos[i].x,
				y : disparos[i].y
		}
		let posRelativa = calcularPosicionRelativa(pos)
		let dimens = {
			x : dimensiones[0],
			y : dimensiones[1]
		}
		if(enCuadro(posRelativa, dimens)) {
			// TODO gradiente de equipo
			espacioProfundo.append('<div class="sprite" style="height:'
					+dimens.x +'px;width:' +dimens.y +'px;left:'
					+posRelativa.x +'px;top:' +posRelativa.y +'px" />')
		}
	}
}

/*
 * Dibuja las naves de otros jugadores y devuelve el nuevo vector de posición
 * que tiene este jugador.
 */
function dibujarNaves(naves, dimensiones) {
	let posJugador
	for(let i = 0; i < disparos.length; i++) {
		if(naves[i].username == username) {
			posJugador = {
					x : naves[i].x,
					y : naves[i].y
			}
			// TODO dibujar energía
			continue
		}
		let pos = {
				x : naves[i].x,
				y : naves[i].y
		}
		let posRelativa = calcularPosicionRelativa(pos)
		let dimens = {
			x : dimensiones[0],
			y : dimensiones[1]
		}
		if(enCuadro(posRelativa, dimens)) {
			// TODO gradiente de equipo
			espacioProfundo.append('<div class="sprite" style="height:'
					+dimens.x +'px;width:' +dimens.y +'px;left:'
					+posRelativa.x +'px;top:' +posRelativa.y +'px" />')
		}
	}
	return posJugador
}

function calcularAngulo(newPos) {
	let angle = Math.atan2(newPos.y -posJugador.y, newPos.x -posJugador.x) *180
		/Math.PI
	posJugador = newPos
	return angle;
}

// Dibuja la nave del jugador y devuelve su sprite
function dibujarJugador() {
	espacioProfundo.append('<img id="jugador" src="/img/ship_01.png" '
			+'style="left:' +margen.x +'px;top:' +margen.y +'px" />')
	return $('#jugador');
}

function giraSprite(sprite, deg) {
	sprite.css('transform', 'rotate(' + deg + 'deg)')
}//giraSprite

function init() {
	posAbsEstrellas = creaCampoDeEstrellas()
	dibujarEspacio()
	dibujarJugador()
	if(idPartida < 0) {
		alert("La partida no está disponible")
		window.location.href = '/verPartida'
		return;
	}
	connect(function(event) {
		if(busy) return;
		busy = true;
		try {
			let bitacoraDelCapitan = JSON.parse(event.body)
			let desplazamiento = bitacoraDelCapitan.desplazamientoJugador[username]
			calcularPosicionCamara(desplazamiento)
			dibujarEspacio()
			dibujarAsteroides(bitacoraDelCapitan.asteroides,
					bitacoraDelCapitan.dimensionAsteroide)
			dibujarDisparos(bitacoraDelCapitan.disparos,
					bitacoraDelCapitan.dimensionDisparo)
			let newPos = dibujarNaves(bitacoraDelCapitan.naves,
					bitacoraDelCapitan.dimensionNave)
			let angle = calcularAngulo(newPos)
			let sprite = dibujarJugador()
			giraSprite(sprite, angle)
			busy = false;
		} catch(err) {
			busy = false;
		}
	}, '/espacioprofundo')
}//init

function accionControles(tecla, inicio, continuamente = false) {
	let eventoWS;
	switch(tecla) {
	case 87 : // 'w'
	case 38 : // flecha arriba
		if(inicio) {
			eventoWS = buildEventoWS(idPartida, continuamente ? 4 : 3,
					tokenSesion, 0)
		} else {
			eventoWS = buildEventoWS(idPartida, 5, tokenSesion)
		}
		break
	case 65 : // 'a'
	case 37 : // flecha derecha
		if(inicio) {
			eventoWS = buildEventoWS(idPartida, continuamente ? 4 : 3,
					tokenSesion, 1)					
		} else {
			eventoWS = buildEventoWS(idPartida, 5, tokenSesion)
		}
		break
	case 68 : // 'd'
	case 39 : // flecha izquierda
		if(inicio) {
			eventoWS = buildEventoWS(idPartida, continuamente ? 4 : 3,
					tokenSesion, 2)			
		} else {
			eventoWS = buildEventoWS(idPartida, 5, tokenSesion)
		}
		break
	case 83 : // 's'
	case 40 : // flecha abajo
		if(inicio) {
			eventoWS = buildEventoWS(idPartida, continuamente ? 4 : 3,
					tokenSesion, 3)
		} else {
			eventoWS = buildEventoWS(idPartida, 5, tokenSesion)
		}
		break
	case 32 :
		if(inicio) {
			eventoWS = buildEventoWS(idPartida, continuamente ? 1 : 0,
					tokenSesion, 3)		
		} else {
			eventoWS = buildEventoWS(idPartida, 2, tokenSesion)
		}
		break
	default :
		return
	}
	sendMessage("/partidaAction", eventoWS)
}

$('body').keydown(function(event) {
	// acción continua
	accionControles(event.which || event.keyCode, true, true)
})
$('body').keyup(function(event) {
	// fin acción contínua
	accionControles(event.which || event.keyCode, false)
})
$('body').keypress(function(event) {
	// una sola acción
	accionControles(event.which || event.keyCode, true)
})
$('body').click(function() {
	// una sola acción
	let eventoWS = buildEventoWS(idPartida, 0, tokenSesion) 
	sendMessage("/partidaAction", eventoWS)
})

$(window).on("unload", disconnect)

$(document).ready(init)
