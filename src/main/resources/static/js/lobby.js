var tablaUsuarios

var numIntegrantesEquipo = {
	0 : 1
}

function borrarPartida(partidaId) {
	let eventoWS = buildEventoWS(partidaId, 5, $("#tokenSesion").val()) 
	sendMessage("/lobbyAction", eventoWS)
}//borrarPartida

function iniciarPartida(partidaId) {
	$('#btn-iniciar').prop('disabled', true)
	let eventoWS = buildEventoWS(partidaId, 4, $("#tokenSesion").val()) 
	sendMessage("/lobbyAction", eventoWS)
}//iniciarPartida

function expulsar(username, idPartida) {
	let eventoWS = buildEventoWS(idPartida, 1, $("#tokenSesion").val(),
			username)
	sendMessage("/lobbyAction", eventoWS)
}

function verificarEquipos() {
	let numEquipos = $('#equiposPartida').val()
	for(let i = 1; i < numEquipos; i++) {
		numIntegrantesEquipo[i] = numIntegrantesEquipo[i] ?
				numIntegrantesEquipo[i] +1 : 1
	}
	let jugadoresEquipos = Object.values(numIntegrantesEquipo);
	let numJugadores = 0;
	for(let i = 0; i < jugadoresEquipos.length; i++) {
		numJugadores += jugadoresEquipos[i]
	}
	$('#btn-iniciar').prop('disabled',
			Object.keys(numIntegrantesEquipo) <= numJugadores)
}

$(window).on("unload", disconnect)

$(document).ready(connectWrapper(function(event) {
		tablaUsuarios = $('#tabla-jugadores')
		let eventoLobby = JSON.parse(event.body)
		switch(eventoLobby.tipoEvento) {
		case 0 : // usuario conectado
			if(eventoLobby.roomid != $('#idPartida').val()) break
			let nuevoJugador =
				$('<tr>', {id : 'jugador-' +eventoLobby.datosAdicionales})
			nuevoJugador.append($('<td>')
								.text(eventoLobby.datosAdicionales))
			nuevoJugador.append($('<td>', {id : 'equipo-jugador-'
				+eventoLobby.datosAdicionales}))
			if(eventoLobby.datosAdicionales != $('#username').val()) {
				nuevoJugador.append($('<td>')
						.append($('<button>', {
							'type' : 'button'
						}).click(function() {
							expulsar(eventoLobby.datosAdicionales, eventoLobby.roomid)
						}).text('Expulsar')))				
			}
			tablaUsuarios.append(nuevoJugador)
			verificarEquipos()
			break
		case 1 : // usuario desconectado
			if(eventoLobby.roomid != $('#idPartida').val()) break
			$('#jugador-' +eventoLobby.datosAdicionales).remove()
			verificarEquipos()
			break
		case 2 : // usuario listo
			console.log("Usuario listo: " +eventoLobby.datosAdicionales)
			break
		case 3 : // usuario no listo
			console.log("Usuario no listo: " +eventoLobby.datosAdicionales)
			break
		case 4 : // iniciar partida
			window.location.href = '/jugar?idPartida=' +eventoLobby.roomid
			break
		case 5 : // borrar partida
			if(eventoLobby.roomid == $('#idPartida').val()) {
				alert("La partida ha sido eliminada")
				window.location.href = "/"
			}
			break
		case 6 : // cambiar de equipo
			if(eventoLobby.roomid == $('#idPartida').val()) {
				$('#equipo-jugador-' +eventoLobby.datosAdicionales)
				.text(eventoLobby.equipos[0])		
				verificarEquipos()
			}
			break
		default :
			console.log("Tipo de evento de lobby desconocido: " +eventoLobby)
		}
	}, '/evento/lobby', verificarEquipos))
