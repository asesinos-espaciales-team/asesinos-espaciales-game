<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page import="mx.asesinosespaciales.ae.modelos.Jugador"%>
<%@page import="mx.asesinosespaciales.ae.modelos.JugadorPK"%>
<%@page import="mx.asesinosespaciales.ae.modelos.Partida"%>
<%@page import="mx.asesinosespaciales.ae.modelos.Usuario"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<t:genericTemplate>

	<jsp:attribute name="head" >
		<title>Mejores puntuaciones - Asesinos Espaciales</title>
		<script src="/webjars/jquery/3.4.1/jquery.min.js" ></script>
		<script src="/js/commons/log.js" ></script>
		<link rel="stylesheet" type="text/css" href="/css/commons/header.css" ></link>
		<link rel="stylesheet" type="text/css" href="/css/commons/footer.css" ></link>
		<link rel="stylesheet" type="text/css" href="/css/commons/log.css" ></link>
	</jsp:attribute>
	
    <jsp:attribute name="header">
    	<jsp:include page="commons/header.jsp" >
    		<jsp:param name="tokenSesion" value="${tokenSesion}" />
    	</jsp:include>
    </jsp:attribute>
    
    <jsp:attribute name="footer">
    	<jsp:include page="commons/footer.jsp" />
    </jsp:attribute>
    
    <jsp:body >
    	<table >
    		<thead>
    			<tr>
	    			<th id="jugador" >
	    				Jugador
	    			</th>
	    			<th >
	    				Partida / Organizador
	    			</th>
	    			<th >
	    				Ganador
	    			</th>
	    			<th id="puntuacion" >
	    				Puntuaci&oacute;n
	    			</th>
	    		</tr>
    		</thead>
    		<tbody>
    			<c:forEach var="jugador" items="${mejoresJugadores}" >
	    			<tr>
	    				<td>
		    				<c:out value="${jugador.id.user.alias}" />
	    				</td>
	    				<td>
	    					<c:out value="${jugador.partida.id}" /> /
	    					<c:out value="${jugador.partida.organizador.alias}" />
	    				</td>
	    				<td>
	    					<c:if test="${jugador.banderaG}" >
	    						<img src="/img/champ.png" alt="Ganador de la partida"
	    						title="Ganador de la partida" />
	    					</c:if>
	    				</td>
	    				<td>
		    				<c:out value="${jugador.puntuacionN}" />
	    				</td>
	    			</tr>
    			</c:forEach>
    		</tbody>
    	</table>
    </jsp:body>

</t:genericTemplate>