<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="mx.asesinosespaciales.ae.util.Vector"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
	"http://www.w3.org/TR/html4/loose.dtd" >
<html lang="es" xml:lang="es" xmlns="http://www.w3.org/1999/xhtml" >
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>ASESINOS ESPACIALES!!</title>
		<script src="/webjars/jquery/3.4.1/jquery.min.js" ></script>
		<script src="/webjars/stomp-websocket/2.3.3-1/stomp.min.js" ></script>
		<script src="/webjars/sockjs-client/1.1.2/sockjs.min.js" ></script>
		<script src="/js/commons/aews.js" ></script>
		<link rel="stylesheet" type="text/css" href="/css/game.css" ></link>
	</head>
	<body>
		<div id="datos" >
			<input id="ancho-espacioprof" type="hidden"
				value="${dimensionesEspacio.getElement(0)}" />
			<input id="largo-espacioprof" type="hidden"
				value="${dimensionesEspacio.getElement(1)}" />
			<input id="username" type="hidden" value="${username}" />
			<!-- TODO cambiar por sesiones seguras de Spring -->
			<input id="tokenSesion" type="hidden" value="${tokenSesion}" />
			<input id="idPartida" type="hidden" value="${idPartida}" />
		</div>
		<div id="campo-estrellas" >
		</div>
		<div id="espacio-profundo" >
		</div>
		<script src="/js/game.js" ></script>
    </body>
</html>