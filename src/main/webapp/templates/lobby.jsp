<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page import="mx.asesinosespaciales.ae.modelos.Jugador"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
	"http://www.w3.org/TR/html4/loose.dtd">
<html lang="es" xml:lang="es" xmlns="http://www.w3.org/1999/xhtml" >
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>Esperando Jugadores - Asesinos Espaciales</title>
		<script src="/webjars/jquery/3.4.1/jquery.min.js" ></script>
		<script src="/webjars/stomp-websocket/2.3.3-1/stomp.min.js" ></script>
		<script src="/webjars/sockjs-client/1.1.2/sockjs.min.js" ></script>
		<script src="/js/commons/aews.js" ></script>
		<script src="/js/lobby.js" ></script>
		<link rel="stylesheet" type="text/css" href="/css/commons/ae.css" ></link>
		<link rel="stylesheet" type="text/css" href="/css/commons/footer.css" ></link>
		<link rel="stylesheet" type="text/css" href="/css/lobby.css" ></link>
	</head>
	<body>
		<div >
			<h2 >Esperando jugadores</h2>
			<div id="jugadores">
				<table>
					<thead>
						<tr>
							<th id="usuario" >Jugador</th>
							<th id="equipo" >Equipo</th>
							<th id="expulsar" >Expulsar</th>
						</tr>
					</thead>
					<tbody id="tabla-jugadores" >
						<c:forEach var="jugador" items="${jugadores}" >
							<tr id="jugador-${jugador.id.user.alias}" >
								<td><c:out value="${jugador.id.user.alias}" /></td>
								<td id="equipo-jugador-${jugador.id.user.alias}" >
									<c:out value="${jugador.equipoN}" />
								</td>
								<c:if test="${jugador.id.user.alias != username}" >
									<td><button type="button"
										onclick="expulsar('${jugador.id.user.alias}', ${idPartida})" >
										Expulsar</button>
									</td>								
								</c:if>
							</tr>
						</c:forEach>
					</tbody>
				</table>
			</div>
		</div>
		<div id="options-panel" >
			<!--  TODO cambiar por sesiones seguras de spring -->
			<input id="tokenSesion" type="hidden" value="${tokenSesion}" />
			<input id="equiposPartida" type="hidden" value="${equiposPartida}" />
			<input id="idPartida" type="hidden" value="${idPartida}" />
			<input id="username" type="hidden" value="${username}" />
			<button type="button" onclick="borrarPartida(${idPartida})" >
				Eliminar</button>
			<button id="btn-iniciar" type="button"
				onclick="iniciarPartida(${idPartida})" disabled >Iniciar</button>
		</div>
		<div id="footer" >
			<jsp:include page="commons/footer.jsp" />
		</div>
	</body>
</html>