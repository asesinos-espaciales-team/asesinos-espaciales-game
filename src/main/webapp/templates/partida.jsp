<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page import="mx.asesinosespaciales.ae.modelos.Partida"%>
<%@page import="mx.asesinosespaciales.ae.modelos.Usuario"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<t:genericTemplate>

	<jsp:attribute name="head" >
		<title>Ver Partida - Asesinos Espaciales</title>
		<script src="/webjars/jquery/3.4.1/jquery.min.js" ></script>
		<script src="/webjars/stomp-websocket/2.3.3-1/stomp.min.js" ></script>
		<script src="/webjars/sockjs-client/1.1.2/sockjs.min.js" ></script>
		<script src="/js/commons/aews.js" ></script>
		<script src="/js/commons/log.js" ></script>
		<script src="/js/game-explorer.js" ></script>
		<link rel="stylesheet" type="text/css" href="/css/commons/header.css" ></link>
		<link rel="stylesheet" type="text/css" href="/css/commons/footer.css" ></link>
		<link rel="stylesheet" type="text/css" href="/css/commons/log.css" ></link>
		<link rel="stylesheet" type="text/css" href="/css/game-explorer.css" ></link>
	</jsp:attribute>
	
	<jsp:attribute name="header">
    	<jsp:include page="commons/header.jsp" >
    		<jsp:param name="tokenSesion" value="${tokenSesion}" />
    	</jsp:include>
    </jsp:attribute>
    
    <jsp:attribute name="footer">
    	<jsp:include page="commons/footer.jsp" />
    </jsp:attribute>
    
    <jsp:body>
		<h2 >Las partidas disponibles son las siguientes</h2>
		<div id="partidas">
			<p>P&aacute;gina ${pagina} de ${totalPaginas}</p>
			<table>
				<thead>
					<tr>
						<th id="usuario" >Organizador</th>
						<th id="opcion" >Opcion de partida</th>
						<th id="equipos" >Equipos disponibles</th>
						<th id="mi-equipo" >Elije un equipo</th>
					</tr>
				</thead>
				<tbody>
					<c:if test="${not empty partidas}" >
						<c:forEach var="partida" items="${partidas}">
							<tr>
								<td>${partida.organizador.alias}</td>
								<td>
									<c:choose>
										<c:when test="${not empty tokenSesion}" >
											<button id="unirseP-${partida.id}"
												name="unirseP" type="button"
												onclick="unirse(${partida.id})" >
												Unirse a la partida</button>
										</c:when>
										<c:otherwise >
											<button name="verP" type="button"
												onclick="verPartida(${partida.id})" >
												Ver Partida</button>
										</c:otherwise>
									</c:choose>
								</td>
								<td >
									<span id="equipos-partida-${partida.id}" ></span>
								</td>
								<td >
									<c:choose>
										<c:when test="${not empty partidaEnCurso and partidaEnCurso.id == partida.id}">
											<select id="mi-equipo-partida-${partida.id}"
												onchange="cambiaEquipo(${partida.id})" >
											</select>
										</c:when>
										<c:otherwise>
											<select id="mi-equipo-partida-${partida.id}" >
											</select>
										</c:otherwise>
									</c:choose>
								</td>
							</tr>
						</c:forEach>
					</c:if>
				</tbody>
			</table>
			<div id="pagination-panel" >
				<c:if test="${pagina > 1}" >
					<button type="button" onclick="previousPage(${pagina})" >
					Anteriores</button>
				</c:if>
				<c:if test="${pagina < totalPaginas}" >
					<button type="button" onclick="nextPage(${pagina})" >
					Siguientes</button>
				</c:if>
			</div>
		</div>
		<!--  TODO reemplazar por sesiones de Spring -->
		<input id="tokenSesion" type="hidden" value="${tokenSesion}" />
		<input id="username" type="hidden" value="${username}" />
		<c:if test="${not empty partidaEnCurso}" >
			<input id="partida-en-curso" type="hidden" value="${partidaEnCurso.id}" />
			<input id="equipo-partida" type="hidden" value="${equiposPartida}" />
		</c:if>
	</jsp:body>
		
</t:genericTemplate>