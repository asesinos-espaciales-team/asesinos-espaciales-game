<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="log-popup-forms.jsp" />
<div id="header-content" >
	<h1 id="header-title" >ASESINOS ESPACIALES!</h1>
	<c:choose>
		<c:when test="${not empty param.tokenSesion}" >
			<button id="header-log-btn" type="button"
				onclick="logout()" >Salir</button>
		</c:when>
		<c:otherwise >
			<button id="header-log-btn" type="button"
				onclick="displayLogForms()" >Iniciar sesi&oacute;n/Registro</button>
		</c:otherwise>
	</c:choose>
</div>