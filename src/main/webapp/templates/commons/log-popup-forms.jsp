<div class="log-popup" id="log-popup-forms" >
	<div class="tab" >
		<button class="tablinks" onclick="openTab(event, 'login-form-container')" >
		Iniciar sesi&oacute;n</button>
		<button class="tablinks" onclick="openTab(event, 'singup-form-container')" >
		Registrarse</button>
	</div>
	
	<div id="login-form-container" class="tabcontent" >
		<form id="login-form" action="#" class="form-container"
			onsubmit="sessionSubmit(this, '/login'); return false" >
			<h2>Login</h2>
			<label for="email" >Correo</label>
			<input type="text" placeholder="alguien@ejemplo.com" name="email" required />
			
			<label for="pwd" >Contrase&ntilde;a</label>
			<input type="password" placeholder="dadada soy Zuckerberg" name="pwd" required />
			
			<button type="submit" class="log-form-btn" >Login</button>
			<button type="button" class="log-form-btn cancel" onclick="closeLogForm()" >Cerrar</button>
		</form>
	</div>
	
	<div id="singup-form-container" class="tabcontent" >
		<form id="singup-form" action="#" class="form-container"
			onsubmit="sessionSubmit(this, '/singup'); return false" >
			<h2>Registro</h2>
			<label for="email" >Correo</label>
			<input type="text" placeholder="alguien@ejemplo.com" name="email" required />
			
			<label for="username" >Nombre de usuario</label>
			<input type="text" placeholder="rockstar69" name="username" required />
			
			<label for="pwd" >Contrase&ntilde;a</label>
			<input type="password" placeholder="dadada soy Zuckerberg" name="pwd" required />
			
			<button type="submit" class="log-form-btn" >Registrame</button>
			<button type="button" class="log-form-btn cancel" onclick="closeLogForm()" >Cerrar</button>
		</form>
	</div>
	
</div>