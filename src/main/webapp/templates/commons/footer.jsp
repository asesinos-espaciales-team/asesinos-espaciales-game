<div id="footer-content" >
	<div >
		<span id="footer-copyright" >&copy; Asesinos espaciales 2019</span>
		<span id="footer-team-members" >Equipo 6: Henry Martinez Bello, Mario
		Alfredo Ortega Rodr�guez y Manuel Ignacio Castillo L�pez.</span>
	</div>
	<div id="footer-about" >Programaci�n Avanzada 2020-I, Maestr�a en Ciencia e
	Ingenier�a de la Computaci�n, UNAM</div>
</div>