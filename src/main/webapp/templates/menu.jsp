<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<t:genericTemplate>

	<jsp:attribute name="head" >
		<title>ASESINOS ESPACIALES!!</title>
		<script src="/webjars/jquery/3.4.1/jquery.min.js" ></script>
		<script src="/js/commons/log.js" ></script>
		<link rel="stylesheet" type="text/css" href="/css/commons/header.css" ></link>
		<link rel="stylesheet" type="text/css" href="/css/commons/footer.css" ></link>
		<link rel="stylesheet" type="text/css" href="/css/commons/log.css" ></link>
		<link rel="stylesheet" type="text/css" href="/css/home.css" ></link>
	</jsp:attribute>
	
    <jsp:attribute name="header">
    	<jsp:include page="commons/header.jsp" >
    		<jsp:param name="tokenSesion" value="${tokenSesion}" />
    	</jsp:include>
    </jsp:attribute>
    
    <jsp:attribute name="footer">
    	<jsp:include page="commons/footer.jsp" />
    </jsp:attribute>
    
    <jsp:body>
        <div id="menu-container" >
        	<c:if test="${not empty tokenSesion}" >
	        	<div>
	        		<button type="button"
	        			onclick="window.location.href = '/lobby'" >Crear partida</button>
	        	</div>
        	</c:if>
        	<div>
        		<button type="button"
        			onclick="window.location.href = '/verPartida'" >Explorar partidas</button>
        	</div>
        	<div>
        		<button type="button"
        			onclick="window.location.href = '/mejoresPuntuaciones'" >
       			Ver puntuaciones m&aacute;s altas</button>
        	</div>
		</div>
		
    </jsp:body>
    
</t:genericTemplate>
