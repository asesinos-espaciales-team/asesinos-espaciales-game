<%@tag description="Plantilla generica" pageEncoding="UTF-8"%>
<%@attribute name="head" fragment="true" %>
<%@attribute name="header" fragment="true" %>
<%@attribute name="footer" fragment="true" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="es" xml:lang="es" xmlns="http://www.w3.org/1999/xhtml" >
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<link rel="stylesheet" type="text/css" href="/css/commons/ae.css" ></link>
		<jsp:invoke fragment="head" />
	</head>
	<body>
		<div id="header" >
			<jsp:invoke fragment="header" />
		</div>
		<div id="body" >
			<jsp:doBody/>
		</div>
		<div id="footer" >
			<jsp:invoke fragment="footer" />
		</div>
	</body>
</html>