package mx.asesinosespaciales.ae.util;

/**
 * Modela un vector n-dimensional sobre un campo subconjunto de los reales.
 * @author <a href="mailto:nachintoch@gmail.com" >Manuel "nachintoch" Castillo</a>
 * @param <T> - El campo subconjunto sobre el que se define el vector. Por ejemplo,
 * para naturales y enteros, podemos usar Integer. Para reales, podemos usar Float o Double.
 */
public interface Vector<T extends Number> {

	/**
	 * Devuelve el i-&eacute;simo elemento del vector
	 * @param i - El elemento deseado
	 * @return El elemento en la i-&eacute;sima posici&oacute;n del vector.
	 * @throws IndexOutOfBoundsException - Si el indice valido no es v&aacute;lido para el vector.
	 * 0 <= i < getDimension()
	 */
	T getElement(int i) throws IndexOutOfBoundsException;
	
	/**
	 * Asigna el elemento dado en la posici&oacute;n indicada del vector.
	 * @param i . El &iacute;ndice en el que se debe insertar el elemento dado.
	 * @param elem - El elemento a agregar en el Vector.
	 * @throws IndexOutOfBoundsException
	 */
	Vector<T> setElement(int i, T elem) throws IndexOutOfBoundsException;
	
	/**
	 * Incrementa el elemento en la i-&eacute;sima localidad con el valor dado. Si el valor es negativo, la localidad
	 * decrementa su valor.
	 * @param i - La localidad a la que se le debe sumar el valor dado.
	 * @param incremento - El incremento o decremento a aplicar en la i-&eacute;sima localidad.
	 */
	Vector<T> incrementar(int i, T incremento);
	
	/**
	 * Indica la dimensi&oacute;n del vector.
	 * @return int - La dimensi&oacute;n del vector.
	 */
	int getDimension();
	
	/**
	 * Suma de vectores
	 * @param u - El vector con el que se suma este. Pueden pertenecer a campos distintos.
	 * @throws IllegalArgumentException - Si u es de dimensi&oacute;n diferente a este.
	 */
	Vector<T> suma(Vector<?> u) throws IllegalArgumentException;
	
	/**
	 * Resta de vectores.
	 * @param u - El vector que se resta a este.
	 * @throws IllegalArgumentException - Si u es de dimensi&oacute;n distinta a este.
	 */
	Vector<T> resta(Vector<?> u) throws IllegalArgumentException;
	
	/**
	 * Devuelve la norma del vector.
	 * @return La magnitud del vector.
	 */
	double norma();
	
	/**
	 * Producto por escalar.
	 * @param escalar - El escalar por el que se van a multiplicar todas las localidades del vector.
	 */
	Vector<T> prodEscalar(T escalar);
	
}//Vector interface
