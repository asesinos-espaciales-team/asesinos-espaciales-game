package mx.asesinosespaciales.ae.util;

import java.util.Objects;

/**
 * Modela un vector n-dimensional sobre los reales.
 * @author <a href="mailto:nachintoch@gmail.com" >Manuel "nachintoch" Castillo</a>
 */
public class VectorND implements Vector<Float> {

	// atributos
	
	private boolean recalcularNorma;
	private double norma;
	
	private final float[] VECTOR;
	
	// constructores
	
	/**
	 * Construye el vector cero de dimensi&oacute;n n.
	 * @param n - La dimensi&oacute;n del vector zero.
	 */
	public VectorND(int n) {
		VECTOR = new float[n];
		norma = 0;
		recalcularNorma = false;
	}
	
	/**
	 * Construye un vector a partir de los n valores xi dados.
	 * @param xi - n valores con los que se formar&aacute; un vector de dimensi&oacute;n n. 
	 */
	public VectorND(float ...xi) {
		this(xi.length);
		System.arraycopy(xi, 0, VECTOR, 0, xi.length);
		recalcularNorma = true;
	}
	
	public VectorND(Vector<?> aCopiar) {
		this(aCopiar.getDimension());
		for(int i = 0; i < aCopiar.getDimension(); i++) {
			this.VECTOR[i] = aCopiar.getElement(i).floatValue();
		}
		recalcularNorma = true;
	}
	
	// métodos

	@Override
	public Float getElement(int i) {
		return VECTOR[i];
	}

	@Override
	public Vector<Float> setElement(int i, Float elem) {
		recalcularNorma |= VECTOR[i] != elem.floatValue();
		VECTOR[i] = elem;
		return this;
	}

	@Override
	public Vector<Float> incrementar(int i, Float incremento) {
		float inc = incremento;
		if(inc == 0) return this;
		recalcularNorma = true;
		VECTOR[i] += inc;
		return this;
	}

	@Override
	public int getDimension() {
		return VECTOR.length;
	}

	@Override
	public Vector<Float> suma(Vector<?> u) throws IllegalArgumentException {
		validateVectorDimension(u);
		for(int i = 0; i < u.getDimension(); i++) {
			incrementar(i, u.getElement(i).floatValue());
		}
		return this;
	}

	@Override
	public Vector<Float> resta(Vector<?> u) throws IllegalArgumentException {
		validateVectorDimension(u);
		for(int i = 0; i < u.getDimension(); i++) {
			incrementar(i, -u.getElement(i).floatValue());
		}
		return this;
	}

	@Override
	public double norma() {
		if(recalcularNorma) {
			norma = 0;
			for(int i = 0; i < getDimension(); i++) {
				float xi = getElement(i);
				norma += xi *xi;
			}
			norma = Math.sqrt(norma);
			recalcularNorma = false;
		}
		return norma;
	}

	@Override
	public Vector<Float> prodEscalar(Float escalar) {
		float e = escalar;
		if(e == 1) return this;
		recalcularNorma = true;
		for(int i = 0; i < getDimension(); i++) {
			VECTOR[i] *= e;
		}
		return this;
	}
	
	@Override
	public int hashCode() {
		return Objects.hash(this.norma(), this.VECTOR);
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public boolean equals(Object obj) {
		if(obj == null) return false;
		if(this == obj) return true;
		if(!(obj instanceof Vector)) return false;
		Vector<Number> u = (Vector<Number>) obj;
		if(u.getDimension() != this.getDimension()) return false;
		for(int i = 0; i < this.getDimension(); i++) {
			if(u.getElement(i) != this.getElement(i)) return false;
		}
		return true;
	}//equals
	
	private void validateVectorDimension(Vector<?> u) throws IllegalArgumentException {
		if(getDimension() != u.getDimension()) {
			throw new IllegalArgumentException("Vector to sum " +u
					+"must have the same dimension as this vector (" +getDimension() +")");
		}
	}
	
	public static VectorND vector2D(float x, float y) {
		return new VectorND(x, y);
	}
	
}
