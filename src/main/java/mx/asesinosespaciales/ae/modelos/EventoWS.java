package mx.asesinosespaciales.ae.modelos;

public abstract class EventoWS {
	protected String tokenSesion;
	protected byte tipoEvento;
	protected Object datosAdicionales;
	protected int roomid;
	
	public String getTokenSesion() {
		return tokenSesion;
	}
	public byte getTipoEvento() {
		return tipoEvento;
	}
	public Object getDatosAdicionales() {
		return datosAdicionales;
	}
	public int getRoomid() {
		return roomid;
	}
	
	public void setTokenSesion(String tokenSesion) {
		this.tokenSesion=tokenSesion;
	}
	public void setTipoEvento(byte tipoEvento) {
		this.tipoEvento=tipoEvento;
	}
	public void setDatosAdicionales(Object datosAdicionales) {
		this.datosAdicionales=datosAdicionales;
	}
	public void setRoomid(int roomid) {
		this.roomid=roomid;
	}
	
}
