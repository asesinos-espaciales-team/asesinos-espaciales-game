package mx.asesinosespaciales.ae.modelos;

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

/**
 * Llave primaria para la clase Jugador.
 * Como usa la anotaci&oacute;n <i>@Embeddable</i> esta clase no se mapea en su
 * propia tabla en la base de datos. En lugar de eso, sus atributos son embebidos;
 * empotrados, concatenados; en la tabla correspondiente a la clase que use esta
 * clase como llave primaria (Jugador es la &uacute;nica clase que usa esta como
 * llave primaria).
 * @author <a href="mailto:nachinotch@gmail.com" >Manuel "nachintoch" Castillo</a>
 * @see Jugador
 * @see Embeddable
 */
@Embeddable
public class JugadorPK implements Serializable {

	/**
	 * Atributo no obligatorio pero recomendado de clase para aquellas que
	 * implementan la interfaz <i>Serializable</i>: un n6uacute;mero de serie
	 * que ayuda a la JVM, programadores y otros frameworks a distinguir distintas
	 * versiones de esta clase.  
	 * @see Serializable
	 */
	private static final long serialVersionUID = -2977750830455632193L;
	
	@ManyToOne
	@JoinColumn(name = "usuario", referencedColumnName = "username")
	private Usuario user;
	
	@Column(name="partida_id")
	private int idPartida;

	// constructores
	
	public JugadorPK() {
		
	}
	
	public JugadorPK(Usuario usuario, int idPartida) {
		user = usuario;
		this.idPartida = idPartida;
	}
	
	// métodos de acceso y modificación
	
	public Usuario getUser() {
		return user;
	}
	public int getIdPartida() {
		return idPartida;
	}
	
	public void setUser(Usuario user) {
		this.user = user;
	}
	public void setIdPartida(int idP) {
		idPartida=idP;
	}
	
	// métodos
	
	@Override
	public int hashCode() {
		return Objects.hash(user, idPartida);
	}
	
	@Override
	public boolean equals(Object obj) {
		if(this == obj) return true;
		if(obj == null || obj.getClass() != this.getClass()) return false;
		JugadorPK otro = (JugadorPK) obj;
		return Objects.equals(user, otro.user) && this.idPartida == otro.idPartida;
	}
	
}//Jugador Primary Key
