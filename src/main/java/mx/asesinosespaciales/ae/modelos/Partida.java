package mx.asesinosespaciales.ae.modelos;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table
public class Partida {
	@Id
	@Column(name="id")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;
	
	@ManyToOne
	@JoinColumn(name = "organizador", referencedColumnName = "username")
	private Usuario organizador;
	
	@Column(name="partidaIniciada")
	private boolean partidaIniciada;
	@Column(name="partidaFinalizada")
	private boolean partidaFinalizada;
	
	@OneToMany(mappedBy = "partida", fetch = FetchType.LAZY)
	private Set<Jugador> jugadores = new HashSet<>();
	
	@Transient
	Set<Usuario> participantes;
	
	//constructores
	public Partida() {
		this.participantes=new HashSet<>();
	}
	public Partida(Integer key,Usuario user,boolean inicio,boolean fin) {
		id=key;
		organizador=user;
		partidaIniciada=inicio;
		partidaFinalizada=fin;
		this.participantes=new HashSet<>();
	}
	//set functions
	public void setId(Integer key) {
		id=key;
	}
	public void setOrganizador(Usuario jOrganizador) {
		organizador=jOrganizador;
	}
	public void setPartidaIniciada(boolean inicio) {
		partidaIniciada=inicio;
	}
	public void setPartidaFinalizada(boolean fin) {
		partidaFinalizada=fin;
	}
	public void setJugadores(Set<Jugador> jugadores) {
		this.jugadores = jugadores;
	}
	//get functions
	public Integer getId() {
		return id;
	}
	public Usuario getOrganizador() {
		return organizador;
	}
	public boolean getPartidaIniciada() {
		return partidaIniciada;
	}
	public boolean getPartidaFinalizada() {
		return partidaFinalizada;
	}
	public Set<Jugador> getJugadores() {
		return jugadores;
	}
	//
	public synchronized boolean agregarParticipante(Usuario jugador) {
		this.participantes.add(jugador);
		return true;
	}
	public synchronized boolean sacarParticipante(Usuario noJugador) {
		for(Usuario item : this.participantes) {
			if(noJugador.getCorreo().equals(item.getCorreo())) {
				this.participantes.remove(item);
			}
		}
		return true;
	}
	
	
}
