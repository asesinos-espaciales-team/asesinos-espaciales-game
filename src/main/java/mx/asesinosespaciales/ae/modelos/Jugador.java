package mx.asesinosespaciales.ae.modelos;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.Table;

@Entity
@Table
public class Jugador {

	@EmbeddedId
	private JugadorPK id;
	
	@ManyToOne
	@MapsId("id")
	private Partida partida;
	
	@Column(name="equipo")
	private int equipoN;
	
	@Column(name="puntuacion")
	private int puntuacionN;
	
	@Column(name="ganador")
	private boolean banderaG;
	/**
	 * Cada variable hace una correspondencia a las columnas de la tabla jugador la cual tomas
	 * como primary key id, la cual viene de la clase JugadorPK.
	 * Asi mismo vemos la relacion que se tiene  con la tabla Partida  la cual es muchos
	 * a una.
	 * */
	
	//constructor
	public Jugador() {
	}
	
	public Jugador(Usuario user,Partida partida,int team,int score,boolean esGanador) {
		id = new JugadorPK(user, partida.getId());
		equipoN=team;
		puntuacionN=score;
		banderaG=esGanador;
	}
	
	//get functions
	
	public int getEquipoN() {
		return equipoN;
	}
	
	public JugadorPK getId() {
		return id;
	}

	public Partida getPartida() {
		return partida;
	}

	public int getPuntuacionN() {
		return puntuacionN;
	}
	public boolean getBanderaG() {
		return banderaG;
	}
	//set functions
	
	public void setId(JugadorPK id) {
		this.id = id;
	}
	
	public void setPartida(Partida lobby) {
		this.partida = lobby;
	}

	public void setEquipoN(int team) {
		equipoN=team;
	}
	public void setPuntuacionN(int score) {
		puntuacionN=score;
	}
	public void setBanderaG(boolean flag) {
		banderaG=flag;
	}
	
	public void sumaPuntos(int puntos) {
		puntuacionN+=puntos;
	}
	/****
	 * Tenemos las funciones set y get para las variables de la clase jugador
	 ** */
	
	@Override
	public boolean equals(Object obj) {
		if(obj == null) return false;
		if(this == obj) return true;
		if(!(obj instanceof Jugador)) return false;
		Jugador otro = (Jugador) obj;
		return this.id.equals(otro.id);
	}
	
	@Override
	public int hashCode() {
		return id.hashCode();
	}
	
}
