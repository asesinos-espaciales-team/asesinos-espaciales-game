package mx.asesinosespaciales.ae.modelos;
import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table
public class Usuario implements Serializable {
	
	private static final long serialVersionUID = -6413638830644486468L;
	
	/**
	 * Indica la vida máxima de la cookie de sesi&oacute;n.
	 * Vive por 7 d&iacute;as. M&aacute;s que una mosca.
	 */
	@Id
	@Column(name="email")
	private String correo;
	
	@Column(name="username", unique=true, nullable=false)
	private String alias;
	
	@Column(name="tokenSesion", unique=true)
	private String token;
	
	@Column(name="password", nullable=false)
	private String contras;
	
	@OneToMany(mappedBy = "organizador", fetch = FetchType.LAZY)
	private Set<Partida> partidas = new HashSet<>();
	
	@OneToMany(mappedBy = "id.user", fetch = FetchType.LAZY)
	private Set<Jugador> jugadores = new HashSet<>();
	
	@Column(name="ultimaSesion")
	@Temporal(TemporalType.TIMESTAMP)
	private Date fecha;
	/**
	 * Cada variable hace una correspondencia a las columnas de la tabla usuario la cual tomas
	 * como primary key email.
	 * Asi mismo vemos la relacion que se tiene  con la tabla Partida y jugadores la cual es una
	 * a muchos para ambas.
	 * */
	//constructores
	public Usuario() {}
	public Usuario(String mail,String user,String tok,String pass,Date fechas) {
		correo=mail;
		alias=user;
		token=tok;
		contras=pass;
		fecha=fechas;
	}
	
	//set functions
	public void setCorreo(String email) {
		correo=email;
	}
	public void setAlias(String user) {
		alias=user;
	}
	public void setToken(String tok) {
		token=tok;
	}
	public void setContras(String pass) {
		contras=pass;
	}
	public void setFecha(Date Fecha) {
		fecha=Fecha;
	}
	public void setPartidas(Set<Partida> partidas) {
		this.partidas = partidas;
	}
	public void setJugadores(Set<Jugador> jugadores) {
		this.jugadores = jugadores;
	}
	//get functions
	public String getCorreo() {
		return correo;
	}
	public String getAlias() {
		return alias;
	}
	public String getToken() {
		return token;
	}
	public String getContras() {
		return contras;
	}
	public Date getFecha() {
		return fecha;
	}
	public Set<Partida> getPartidas() {
		return partidas;
	}
	public Set<Jugador> getJugadores() {
		return jugadores;
	}
	/****
	 * Tenemos las funciones set y get para las variables de la clase usuario
	 ** */
	
}
