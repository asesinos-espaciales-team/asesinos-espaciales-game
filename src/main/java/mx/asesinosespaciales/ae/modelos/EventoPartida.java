package mx.asesinosespaciales.ae.modelos;

import mx.asesinosespaciales.ae.galaxia.NaveJugador;
import mx.asesinosespaciales.ae.util.Vector;
import mx.asesinosespaciales.ae.util.VectorND;

public class EventoPartida extends EventoWS {

	public static final byte TIPO_ACCION_DISPARO = 0;
	
	public static final byte TIPO_ACCION_DISPARO_RAPIDO = 1;
	
	public static final byte TIPO_ACCION_ALTO_AL_FUEGO = 2;
	
	public static final byte TIPO_ACCION_ACELERACION = 3;
	
	public static final byte TIPO_ACCION_ACELERACION_SOSTENIDA = 4;
	
	public static final byte TIPO_ACCION_ACELERACION_CERO = 5;
	
	public static final byte COD_ACCEL_ARRIBA = 0;
	
	public static final byte COD_ACCEL_DERECHA = 1;
	
	public static final byte COD_ACCEL_IZQUIERDA = 2;
	
	public static final byte COD_ACCEL_ABAJO = 3;
	
	public Vector<Float> decodificaAceleracion() {
		switch(((Integer) super.datosAdicionales).byteValue()) {
		case EventoPartida.COD_ACCEL_ABAJO :
			return VectorND.vector2D(0, NaveJugador.ACELERACION_MOTORES);
		case EventoPartida.COD_ACCEL_ARRIBA :
			return VectorND.vector2D(0, -NaveJugador.ACELERACION_MOTORES);
		case EventoPartida.COD_ACCEL_DERECHA :
			return VectorND.vector2D(-NaveJugador.ACELERACION_MOTORES, 0);
		case EventoPartida.COD_ACCEL_IZQUIERDA :
			return VectorND.vector2D(NaveJugador.ACELERACION_MOTORES, 0);
		default :
			throw new IllegalStateException("El codigo de aceleraci\u00F3n no es v\u00E1lido");
		}
	}
	
}
