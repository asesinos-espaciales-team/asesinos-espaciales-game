package mx.asesinosespaciales.ae.modelos;

public class EventoLobby extends EventoWS {
	
	private Integer[] equipos;
	
	public static final byte TIPO_USUARIO_CONECTADO = 0;
	
	public static final byte TIPO_USUARIO_DESCONECTADO = 1;
	
	public static final byte TIPO_USUARIO_LISTO = 2;
	
	public static final byte TIPO_USUARIO_NOLISTO = 3;
	
	public static final byte TIPO_INICIAR_PARTIDA = 4;
	
	public static final byte TIPO_BORRAR_PARTIDA = 5;
	
	public static final byte TIPO_CAMBIAR_EQUIPO = 6;

	public Integer[] getEquipos() {
		return equipos;
	}

	public void setEquipos(Integer[] numeroEquipos) {
		this.equipos = numeroEquipos;
	}
	
}//Evento Loddy
