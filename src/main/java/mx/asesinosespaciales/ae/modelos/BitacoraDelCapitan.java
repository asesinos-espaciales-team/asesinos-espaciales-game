package mx.asesinosespaciales.ae.modelos;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Mensaje que contiene toda la informaci&oacute;n que necesitan los clientes de los jugadores para poder representar
 * el mundo del juego en un tiempo dado.
 * @author <a href="mailto:nachintoch@gmail.com" >Manuel "nachintoch" Castillo</a>
 */
public class BitacoraDelCapitan {

	private float[][] asteroides;
	
	private List<Sprite> naves;
	
	private List<Sprite> disparos;
	
	private float[] dimensionAsteroide;
	
	private float[] dimensionNave;
	
	private float[] dimensionDisparo;
	
	private boolean partidaTerminada;
	
	private String mejorJugador;
	
	private long mejorPuntuacion;
	
	private Map<String, float[]> desplazamientoJugador = new ConcurrentHashMap<>();
	
	private Map<String, String> mensajes = new ConcurrentHashMap<>(); 

	public float[][] getAsteroides() {
		return asteroides;
	}

	public void setAsteroides(float[][] asteroides) {
		this.asteroides = asteroides;
	}

	public List<Sprite> getNaves() {
		return naves;
	}

	public void setNaves(List<Sprite> naves) {
		this.naves = naves;
	}

	public List<Sprite> getDisparos() {
		return disparos;
	}

	public void setDisparos(List<Sprite> disparos) {
		this.disparos = disparos;
	}

	public float[] getDimensionAsteroide() {
		return dimensionAsteroide;
	}

	public void setDimensionAsteroide(float[] dimensionAsteroide) {
		this.dimensionAsteroide = dimensionAsteroide;
	}

	public float[] getDimensionNave() {
		return dimensionNave;
	}

	public void setDimensionNave(float[] dimensionNave) {
		this.dimensionNave = dimensionNave;
	}

	public float[] getDimensionDisparo() {
		return dimensionDisparo;
	}

	public void setDimensionDisparo(float[] dimensionDisparo) {
		this.dimensionDisparo = dimensionDisparo;
	}

	public boolean isPartidaTerminada() {
		return partidaTerminada;
	}

	public void setPartidaTerminada(boolean partidaTerminada) {
		this.partidaTerminada = partidaTerminada;
	}

	public Map<String, String> getMENSAJES() {
		return mensajes;
	}
	
	public void encolarMensaje(String usuarioRemitente, String mensaje) {
		mensajes.put(usuarioRemitente, mensaje);
	}
	
	public void partidaFinalizada() {
		partidaTerminada = true;
	}

	public String getMejorJugador() {
		return mejorJugador;
	}

	public void setMejorJugador(String mejorJugador) {
		this.mejorJugador = mejorJugador;
	}

	public long getMejorPuntuacion() {
		return mejorPuntuacion;
	}

	public void setMejorPuntuacion(long mejorPuntuacion) {
		this.mejorPuntuacion = mejorPuntuacion;
	}

	public Map<String, float[]> getDesplazamientoJugador() {
		return desplazamientoJugador;
	}

	public void setDesplazamientoJugador(Map<String, float[]> desplazamientoJugador) {
		this.desplazamientoJugador = desplazamientoJugador;
	}
	
	public void registrarDesplazamiento(String username, float[] desplazamiento) {
		this.desplazamientoJugador.put(username, desplazamiento);
	}
	
}//Espacio Profundo
