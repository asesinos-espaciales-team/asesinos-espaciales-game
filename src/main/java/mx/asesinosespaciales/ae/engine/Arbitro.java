package mx.asesinosespaciales.ae.engine;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Random;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ConcurrentMap;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.transaction.annotation.Transactional;

import mx.asesinosespaciales.ae.AsesinosEspaciales;
import mx.asesinosespaciales.ae.controladores.ConfiguracionWebSocket;
import mx.asesinosespaciales.ae.db.DAOJugador;
import mx.asesinosespaciales.ae.db.DAOPartida;
import mx.asesinosespaciales.ae.galaxia.Asteroide;
import mx.asesinosespaciales.ae.galaxia.Municion;
import mx.asesinosespaciales.ae.galaxia.NaveJugador;
import mx.asesinosespaciales.ae.modelos.BitacoraDelCapitan;
import mx.asesinosespaciales.ae.modelos.Jugador;
import mx.asesinosespaciales.ae.modelos.Partida;
import mx.asesinosespaciales.ae.modelos.Sprite;
import mx.asesinosespaciales.ae.util.Vector;
import mx.asesinosespaciales.ae.util.VectorND;

/**
 * Controla la ejecuci&oacute;n de una partida.
 * @since Asesinos Espaciales 2.0
 */
public class Arbitro implements Runnable {

	// atributos

	@Autowired
	private SimpMessagingTemplate webSocket;
	
	@Autowired
	private DAOJugador daoJugador;
	
	@Autowired
	private DAOPartida daoPartida;
	
	private BitacoraDelCapitan bitacoraDelCapitan;
	
	private Jugador mejorJugador;
	
	private long mejorPuntuacion;
	
	private boolean finDelJuego;
	
	private final int ID_PARTIDA;
	
	/**
	 * Asocia un n&uacute;mero de equipo con el n&uacute;mero de integrantes en &eacute;l.
	 */
	private final Map<Integer, Integer> NUM_INTEGRANTES_EQUIPOS;
	
	/**
	 * HashMap con todos los jugadores participantes en la partida en curso.
	 * Relaciona: username (String) - NaveJugador
	 */
	private final List<NaveJugador> JUGADORES;
	
	/**
	 * HashMap con los jugadores a&uacute;n sin descalificar y que permanecen activos en la partida curso.
	 * Relaciona: username (String) - NaveJugador
	 */
	private final ConcurrentMap<String, NaveJugador> JUGADORES_ACTIVOS;
	
	private final Queue<Municion> DISPAROS = new ConcurrentLinkedQueue<>();
	
	private final List<Asteroide> ASTEROIDES;
	
	/**
	 * Asocia username con vector hacia donde acelera continuamente
	 */
	private final ConcurrentMap<String, Vector<Float>> JUGADORES_CON_ACCEL_CONTINUA;
	
	/**
	 * Asocia usernames con el número de peticiones realizadas para ejecutar un disparo rápido
	 */
	private final ConcurrentMap<String, Byte> JUGADORES_CON_DISPARO_RAPIDO;
	
	/**
	 * Milisegundos que duerme este hilo entre actualizaciones del estado de la partida.
	 */
	// FIXME probar que esta tasa produzca una animación fluida
	public static final long TASA_ACTIALIZACION = 50;
	
	private static final long PETICIONES_MIN_POR_DISPARO_RAPIDO = 1000 /TASA_ACTIALIZACION;
	
	// FIXME probar que las dimensiones del mundo sean adecuadaas
	public static final VectorND DIMENSIONES_MUNDO = VectorND.vector2D(5000, 5000);
	
	// FIXME probar que este sea un buen n&uacute;mero de asteroides
	public static final short MAXIMOS_ASTEROIDES = 50;
	
	// constructores
	
	public Arbitro(int partidaId, List<Jugador> jugadores) {
		finDelJuego = false;
		Random r = new Random();
		int numeroAsteroides = r.nextInt(MAXIMOS_ASTEROIDES) +1;
		ID_PARTIDA = partidaId;
		ASTEROIDES = generaAsteroides(r, numeroAsteroides);
		JUGADORES = new ArrayList<>(jugadores.size());
		JUGADORES_ACTIVOS = new ConcurrentHashMap<>(jugadores.size());
		NUM_INTEGRANTES_EQUIPOS = preparaJugadores(jugadores, r, JUGADORES, JUGADORES_ACTIVOS);
		mejorJugador = JUGADORES.get(0).PROPIETARIO;
		JUGADORES_CON_ACCEL_CONTINUA = new ConcurrentHashMap<>(jugadores.size());
		JUGADORES_CON_DISPARO_RAPIDO = new ConcurrentHashMap<>(jugadores.size());
	}//constructor
	
	// métodos
	
	public void registrarDisparo(String username, boolean disparoRapido) {
		NaveJugador jugador = JUGADORES_ACTIVOS.get(username);
		if(jugador == null) return;
		Municion disparo = new Municion(jugador.EQUIPO, jugador.PROPIETARIO,
				jugador.getPosicionInstantanea().getElement(0),
				jugador.getPosicionInstantanea().getElement(1), 55);
		DISPAROS.add(disparo);
		if(disparoRapido) {
			JUGADORES_CON_DISPARO_RAPIDO.put(username, (byte) 0);
		}
	}//registrarDisparo
	
	public void acelerarJugador(String username, Vector<Float> aceleracion, boolean continuamente) {
		NaveJugador jugador = JUGADORES_ACTIVOS.get(username);
		if(jugador == null) return;
		if(continuamente) JUGADORES_CON_ACCEL_CONTINUA.put(username, aceleracion);
		jugador.acelerar(aceleracion, TASA_ACTIALIZACION);
	}//acelerarJugador
	
	public void altoAlFuego(String username) {
		JUGADORES_CON_DISPARO_RAPIDO.remove(username);
	}//altoAlFuego
	
	public void dejarAcelerar(String username) {
		JUGADORES_CON_ACCEL_CONTINUA.remove(username);
	}//dejarAcelerar
	
	@Override
	public void run() {
		final float[] dimensionesAsteroide = new float[] {64, 64};
		final float[] dimensionesNave = new float[] {72, 51};
		final float[] dimensionesDisparo = new float[] {10, 10};
		while(!finDelJuego) {
			bitacoraDelCapitan = new BitacoraDelCapitan();
			bitacoraDelCapitan.setMejorJugador(mejorJugador.getId().getUser().getAlias());
			bitacoraDelCapitan.setMejorPuntuacion(mejorPuntuacion);
			bitacoraDelCapitan.setDimensionAsteroide(dimensionesAsteroide);
			bitacoraDelCapitan.setDimensionDisparo(dimensionesDisparo);
			bitacoraDelCapitan.setDimensionNave(dimensionesNave);
			Collection<NaveJugador> jugadores = JUGADORES_ACTIVOS.values();
			for(NaveJugador jugador : jugadores) {
				colisionesConMuniciones(jugador);
				colisionesConAsteroides(jugador);
			}
			moverDisparos();
			calcularAceleracionContinua();
			generarDisparosRapidos();
			moverJugadores(jugadores);
			enviarCambios();
			try {
				Thread.sleep(TASA_ACTIALIZACION);
			} catch(InterruptedException e) {
				Thread currentThread = Thread.currentThread();
				currentThread.interrupt();
				Logger.getLogger(Arbitro.class.getSimpleName()).log(Level.INFO,
						"Arbitro thread " +currentThread.getName() +" interrupted");
			}
		}//mientras la partida no haya terminado
		enviarCambios();
		for(NaveJugador jugador : JUGADORES) {
			registrarPuntuacion(jugador);
		}
	}//run
	
	private void colisionesConMuniciones(NaveJugador jugador) {
		for(Iterator<Municion> disparos = DISPAROS.iterator(); disparos.hasNext();) {
			Municion disparo = disparos.next();
			if(jugador.EQUIPO == disparo.EQUIPO) continue;
			if(jugador.compruebaColision(disparo)) {
				impactarJugador(jugador, (byte) 1, false);
				disparo.PROPIETARIO.sumaPuntos(100);
				if(disparo.PROPIETARIO.getPuntuacionN() > mejorPuntuacion) {
					mejorPuntuacion = disparo.PROPIETARIO.getPuntuacionN();
					mejorJugador = disparo.PROPIETARIO;
					bitacoraDelCapitan.setMejorJugador(mejorJugador.getId().getUser().getAlias());
					bitacoraDelCapitan.setMejorPuntuacion(mejorPuntuacion);
				}
				disparos.remove();
			}
		}
	}//colisionesConMuniciones
	
	private void colisionesConAsteroides(NaveJugador jugador) {
		for(int i = 0; i < ASTEROIDES.size(); i++) {
			if(jugador.compruebaColision(ASTEROIDES.get(i))) {
				impactarJugador(jugador, (byte) 1, true);
			}
		}
	}//colisionesConAsteroides
	
	private void moverDisparos() {
		List<Sprite> disparos = new ArrayList<>(DISPAROS.size());
		for(Municion disparo : DISPAROS) {
			disparo.moverse(TASA_ACTIALIZACION);
			Sprite sprite = new Sprite();
			sprite.setEquipo(disparo.EQUIPO);
			sprite.setUsername(disparo.PROPIETARIO.getId().getUser().getAlias());
			sprite.setX(disparo.getPosicionInstantanea().getElement(0));
			sprite.setY(disparo.getPosicionInstantanea().getElement(1));
			disparos.add(sprite);
		}
		bitacoraDelCapitan.setDisparos(disparos);
	}//moverDisparos
	
	private void calcularAceleracionContinua() {
		for(Map.Entry<String, Vector<Float>> jugadorAccel : JUGADORES_CON_ACCEL_CONTINUA.entrySet()) {
			NaveJugador jugador = JUGADORES_ACTIVOS.get(jugadorAccel.getKey());
			if(jugador == null) {
				JUGADORES_CON_ACCEL_CONTINUA.remove(jugadorAccel.getKey());
				continue;
			}
			Vector<Float> velocidadAnterior = jugador.getVelocidad();
			jugador.acelerar(jugadorAccel.getValue(), TASA_ACTIALIZACION);
			if(velocidadAnterior.equals(jugador.getVelocidad())) {
				JUGADORES_CON_ACCEL_CONTINUA.remove(jugadorAccel.getKey());
			}
		}
	}//calcularAceleracionContinua
	
	private void generarDisparosRapidos() {
		for(Map.Entry<String, Byte> jugadorDisp : JUGADORES_CON_DISPARO_RAPIDO.entrySet()) {
			NaveJugador jugador = JUGADORES_ACTIVOS.get(jugadorDisp.getKey());
			if(jugador == null) {
				JUGADORES_CON_DISPARO_RAPIDO.remove(jugadorDisp.getKey());
				continue;
			}
			byte peticiones = jugadorDisp.getValue();
			if(++peticiones < Arbitro.PETICIONES_MIN_POR_DISPARO_RAPIDO) {
				JUGADORES_CON_DISPARO_RAPIDO.put(jugadorDisp.getKey(), peticiones);
			} else {
				Municion disparo = new Municion(jugador.EQUIPO, jugador.PROPIETARIO,
						jugador.getPosicionInstantanea().getElement(0),
						jugador.getPosicionInstantanea().getElement(1), 55);
				DISPAROS.add(disparo);
				JUGADORES_CON_DISPARO_RAPIDO.put(jugadorDisp.getKey(), (byte) 0);
			}
		}
	}
	
	private void moverJugadores(Collection<NaveJugador> jugadores) {
		List<Sprite> naves = new ArrayList<>(jugadores.size());
		for(NaveJugador jugador : jugadores) {
			float[] desplazamiento = new float[] {jugador.getPosicionInstantanea().getElement(0),
					jugador.getPosicionInstantanea().getElement(0)};
			jugador.calcularSiguientePosicion(TASA_ACTIALIZACION);
			Sprite sprite = new Sprite();
			sprite.setEnergia(jugador.getEnergia());
			sprite.setEquipo(jugador.EQUIPO);
			sprite.setUsername(jugador.PROPIETARIO.getId().getUser().getAlias());
			sprite.setX(jugador.getPosicionInstantanea().getElement(0));
			sprite.setY(jugador.getPosicionInstantanea().getElement(1));
			desplazamiento[0] = desplazamiento[0] -jugador.getPosicionInstantanea().getElement(0);
			desplazamiento[1] = desplazamiento[1] -jugador.getPosicionInstantanea().getElement(1);
			bitacoraDelCapitan.registrarDesplazamiento(jugador.PROPIETARIO.getId().getUser().getAlias(),
					desplazamiento);
			naves.add(sprite);
		}
		bitacoraDelCapitan.setNaves(naves);
	}//moverJugador
	
	private void enviarCambios() {
		webSocket.convertAndSend(ConfiguracionWebSocket.RUTA_BROKER_MENSAJERIA +"/espacioprofundo", bitacoraDelCapitan);
	}//enviarCambios
	
	@Transactional
	private void registrarPuntuacion(NaveJugador jugador) {
		jugador.PROPIETARIO.setBanderaG(mejorJugador.equals(jugador.PROPIETARIO));
		daoJugador.actualizar(jugador.PROPIETARIO);
	}//enviarCambios
	
	private static float generarPosicionAleatoreaEnRango(float a, float b, Random r) {
		return a +r.nextFloat() *b;
	}//generarPosicionAleatoreaEnRango
	
	private static List<Asteroide> generaAsteroides(Random r, int numeroAsteroides) {
		List<Asteroide> asteroides = new ArrayList<>(numeroAsteroides);
		for(int i = 0; i < numeroAsteroides; i++) {
			float x = generarPosicionAleatoreaEnRango(0, DIMENSIONES_MUNDO.getElement(0), r);
			float y = generarPosicionAleatoreaEnRango(0, DIMENSIONES_MUNDO.getElement(1), r);
			Asteroide asteroide = new Asteroide(-1, null, x, y, 0);
			boolean empalmado = false;
			for(Asteroide otro : asteroides) {
				if(otro.compruebaColision(asteroide)) {
					empalmado = true;
					break;
				}
			}
			if(!empalmado) asteroides.add(asteroide);
		}
		return asteroides;
	}//generaAsteroides
	
	private static Map<Integer, Integer> preparaJugadores(List<Jugador> jugadores, Random r, List<NaveJugador> naves,
			ConcurrentMap<String, NaveJugador> jugadoresActivos) {
		HashMap<Integer, VectorND> equiposCentroides = new HashMap<>();
		Map<Integer, Integer> numIntegrantesEquipos = new HashMap<>();
		final int maxDistanciaEquipo = 51 *5;
		for(Jugador jugador : jugadores) {
			VectorND centroideEquipo = equiposCentroides.get(jugador.getEquipoN());
			float x;
			float y;
			if(centroideEquipo == null) {
				x = generarPosicionAleatoreaEnRango(0, DIMENSIONES_MUNDO.getElement(0), r);
				y = generarPosicionAleatoreaEnRango(0, DIMENSIONES_MUNDO.getElement(1), r);
				centroideEquipo = VectorND.vector2D(x, y);
				equiposCentroides.put(jugador.getEquipoN(), centroideEquipo);
				numIntegrantesEquipos.put(jugador.getEquipoN(), 0);
			} else {
				numIntegrantesEquipos.put(jugador.getEquipoN(), numIntegrantesEquipos.get(jugador.getEquipoN()) +1);
			}//si no se ha generado un centroide para este equipo
			x = centroideEquipo.getElement(0);
			float a = x >= maxDistanciaEquipo ? x -maxDistanciaEquipo : 0;
			float b = DIMENSIONES_MUNDO.getElement(0) >= x +maxDistanciaEquipo ? x +maxDistanciaEquipo : x;
			x = generarPosicionAleatoreaEnRango(a, b, r);
			y = centroideEquipo.getElement(1);
			a = y >= maxDistanciaEquipo ? y -maxDistanciaEquipo : 0;
			b = DIMENSIONES_MUNDO.getElement(1) >= y +maxDistanciaEquipo ? y +maxDistanciaEquipo : y;
			y = generarPosicionAleatoreaEnRango(a, b, r);
			VectorND posInit = VectorND.vector2D(x, y);
			// TODO probar que esta velocidad produzca una animación fluida
			NaveJugador nave = new NaveJugador(jugador.getEquipoN(), jugador, posInit.getElement(0),
					posInit.getElement(1), 50);
			naves.add(nave);
			jugadoresActivos.put(jugador.getId().getUser().getAlias(), nave);
		}//agrega a los jugadores a la partida
		return numIntegrantesEquipos;
	}//preparaJugadores
	
	@Transactional
	private void impactarJugador(NaveJugador jugador, byte damage, boolean rebotar) {
		jugador.impactar(damage);
		if(jugador.estaDescalificado()) {
			JUGADORES_ACTIVOS.remove(jugador.PROPIETARIO.getId().getUser().getAlias());
			int integrantesRestantes = NUM_INTEGRANTES_EQUIPOS.get(jugador.EQUIPO);
			if(integrantesRestantes > 1) {
				NUM_INTEGRANTES_EQUIPOS.put(jugador.EQUIPO, --integrantesRestantes);
			} else {
				NUM_INTEGRANTES_EQUIPOS.remove(jugador.EQUIPO);
				if(NUM_INTEGRANTES_EQUIPOS.keySet().size() <= 1) {
					finDelJuego = true;
					Partida partida = daoPartida.buscar(ID_PARTIDA);
					partida.setPartidaFinalizada(true);
					daoPartida.actualizar(partida);
					AsesinosEspaciales.PARTIDAS_ACTIVAS.remove(ID_PARTIDA);
				}
			}
		} else if(rebotar) {
			jugador.acelerar(jugador.getVelocidad().prodEscalar(-1f), TASA_ACTIALIZACION);
		}
	}//impactarJugador

}//clase Arbitro
