package mx.asesinosespaciales.ae;

import java.util.Properties;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.dao.annotation.PersistenceExceptionTranslationPostProcessor;
import org.springframework.orm.hibernate5.HibernateTransactionManager;
import org.springframework.orm.hibernate5.LocalSessionFactoryBean;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import mx.asesinosespaciales.ae.engine.Arbitro;
import mx.asesinosespaciales.ae.modelos.Jugador;
import mx.asesinosespaciales.ae.modelos.Partida;
import mx.asesinosespaciales.ae.modelos.Usuario;

/**
 * Clase principal que inicia la ejecuci&oacute;n del servidor de la aplicaci&oacute;n web <b><i>Asesinos Espaciales
 * </i></b>
 */
@Configuration
@EnableTransactionManagement
@ComponentScan(basePackages="mx.asesinosespaciales.ae")
@SpringBootApplication(exclude={DataSourceAutoConfiguration.class})
public class AsesinosEspaciales {
	
	public static final ExecutorService FABRICA_HILOS = Executors.newCachedThreadPool();
	
	public static final ConcurrentMap<Integer, Arbitro> PARTIDAS_ACTIVAS = new ConcurrentHashMap<>();
	
	@Bean
	public LocalSessionFactoryBean sesionFactory() {
		LocalSessionFactoryBean fabricaLocalSesiones = new LocalSessionFactoryBean();
		fabricaLocalSesiones.setDataSource(dataSource());
		Properties propiedades = new Properties();
		propiedades.setProperty("connection.pool_size", "1");
		propiedades.setProperty("hibernate.dialect", "org.hibernate.dialect.MySQL5Dialect");
		propiedades.setProperty("hibernate.enable_lazy_load_no_trans", "true");
		propiedades.setProperty("spring.jpa.show-sql", "true");
		fabricaLocalSesiones.setHibernateProperties(propiedades);
		fabricaLocalSesiones.setAnnotatedClasses(Jugador.class, Partida.class, Usuario.class);
		return fabricaLocalSesiones;
	}
	
	@Bean
	public HibernateTransactionManager transactionManager() {
		HibernateTransactionManager transactionManager = new HibernateTransactionManager();
		transactionManager.setSessionFactory(sesionFactory().getObject());
		return transactionManager;
	}
	
	@Bean
	public DataSource dataSource() {
		return DataSourceBuilder.create()
				.driverClassName("com.mysql.cj.jdbc.Driver")
				.url("jdbc:mysql://localhost:3306/as_espaciales?serverTimezone=UTC")
				.username("astroasesino")
				.password("3lK41As3r")
				.build();
	}
	
	@Bean
	public PlatformTransactionManager transactionManager(EntityManagerFactory emf) {
	    JpaTransactionManager transactionManager = new JpaTransactionManager();
	    transactionManager.setEntityManagerFactory(emf);
	    return transactionManager;
	}
	 
	@Bean
	public PersistenceExceptionTranslationPostProcessor exceptionTranslation() {
	    return new PersistenceExceptionTranslationPostProcessor();
	}
	
	// main
	
	/**
	 * Pone en marcha el servidor de <i>Asesinos Espaciales</i>
	 * @param args - Argumentos de consola que se le proporcionaran.
	 */
    public static void main( String[] args ) {
        SpringApplication.run(AsesinosEspaciales.class, args);
    }//main
    
}//AsesinosEspaciales
