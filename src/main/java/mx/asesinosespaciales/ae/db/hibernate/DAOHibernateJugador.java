package mx.asesinosespaciales.ae.db.hibernate;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.stereotype.Repository;

import mx.asesinosespaciales.ae.db.DAOJugador;
import mx.asesinosespaciales.ae.modelos.Jugador;
import mx.asesinosespaciales.ae.modelos.JugadorPK;

@Repository
public class DAOHibernateJugador extends DAOHibernateGenerico<JugadorPK, Jugador> implements DAOJugador {

	public DAOHibernateJugador() {
		super(Jugador.class);
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<Jugador> buscarMejoresPuntuaciones(int limit) {
		CriteriaBuilder cb = entityManager.getCriteriaBuilder();
		CriteriaQuery<Jugador> criteriaQ = cb.createQuery(CLAZZ);
		Root<Jugador> raiz = criteriaQ.from(CLAZZ);
		Predicate noCero = cb.greaterThan(raiz.get("puntuacionN"), 0);
		criteriaQ.select(raiz).where(noCero).orderBy(cb.desc(raiz.get("puntuacionN")));
		Query query = entityManager.createQuery(criteriaQ);
		query.setMaxResults(limit);
		return query.getResultList();
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<Integer> equiposEnPartida(int idPartida) {
		CriteriaBuilder cb = entityManager.getCriteriaBuilder();
		CriteriaQuery<Integer> queryEquipos = cb.createQuery(Integer.class);
		Root<Jugador> raiz = queryEquipos.from(CLAZZ);
		Predicate laPartida = cb.equal(raiz.get("id").get("idPartida"), idPartida);
		queryEquipos.select(raiz.get("equipoN")).distinct(true).where(laPartida);
		Query query = entityManager.createQuery(queryEquipos);
		try {
			return query.getResultList();
		} catch(NoResultException e) {
			return new ArrayList<>(0);
		}
	}//cuentaEquiposEnPartida

	@Override
	@SuppressWarnings("unchecked")
	public List<Integer> idPartidasNoGanadasConElUsuario(String username) {
		CriteriaBuilder cb = entityManager.getCriteriaBuilder();
		CriteriaQuery<Integer> queryEquipos = cb.createQuery(Integer.class);
		Root<Jugador> raiz = queryEquipos.from(CLAZZ);
		Predicate esPartida = cb.equal(raiz.get("id").get("user").get("alias"), username);
		Predicate noGanador = cb.isFalse(raiz.get("banderaG"));
		queryEquipos.select(raiz.get("id").get("idPartida")).where(esPartida, noGanador)
			.orderBy(cb.asc(raiz.get("id")));
		Query query = entityManager.createQuery(queryEquipos);
		try {
			return query.getResultList();
		} catch(NoResultException e) {
			return new ArrayList<>(0);
		}
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<Jugador> buscarJugadoresEnPartida(int idPartida) {
		CriteriaBuilder cb = entityManager.getCriteriaBuilder();
		CriteriaQuery<Jugador> queryJugadores = cb.createQuery(CLAZZ);
		Root<Jugador> raiz = queryJugadores.from(CLAZZ);
		Predicate esPartida = cb.equal(raiz.get("id").get("idPartida"), idPartida);
		queryJugadores.select(raiz).where(esPartida);
		Query query = entityManager.createQuery(queryJugadores);
		try {
			return query.getResultList();
		} catch(NoResultException e) {
			return new ArrayList<>(0);
		}
	}

}//DAOHibernateJugador
