package mx.asesinosespaciales.ae.db.hibernate;

import javax.persistence.NoResultException;
import javax.persistence.Query;

import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Repository;

import mx.asesinosespaciales.ae.db.DAOUsuario;
import mx.asesinosespaciales.ae.modelos.Usuario;

@Repository
public class DAOHibernateUsuario extends DAOHibernateGenerico<String, Usuario> implements DAOUsuario {

	// constructor
	
	public DAOHibernateUsuario() {
		super(Usuario.class);
	}

	// métodos
	
	@Override
	public void borrarPorUsername(String username) {
		Usuario usr = buscarPorUsername(username);
		if(usr == null) return;
		borrar(usr);
	}

	@Override
	public void borrarPorTokenSesion(String token) {
		Usuario usr = buscarPorTokenSesion(token);
		if(usr == null) return;
		borrar(usr);
	}

	@Override
	public Usuario buscarPorUsername(String username) {
		try {
			Query  query = construirSelectConFiltro("username", username);
			return (Usuario) query.getSingleResult();
		} catch(EmptyResultDataAccessException | NoResultException e) {
			return null;
		}
	}

	@Override
	public Usuario buscarPorTokenSesion(String token) {
		try {
			Query query = construirSelectConFiltro("token", token);
			return (Usuario) query.getSingleResult();
		} catch(EmptyResultDataAccessException | NoResultException e) {
			return null;
		}
	}
	
	private Query construirSelectConFiltro(final String nombreColumna, final Object valorEsperado) {
		Query query = entityManager.createQuery("from " +Usuario.class.getName() +" where "
				+nombreColumna +" = :" +nombreColumna);
		query.setParameter(nombreColumna, valorEsperado);
		return query;
	}

}
