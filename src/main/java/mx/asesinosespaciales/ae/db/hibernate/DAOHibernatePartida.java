package mx.asesinosespaciales.ae.db.hibernate;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.stereotype.Repository;

import mx.asesinosespaciales.ae.db.DAOPartida;
import mx.asesinosespaciales.ae.modelos.Partida;

@Repository
public class DAOHibernatePartida extends DAOHibernateGenerico<Integer, Partida> implements DAOPartida {

	public DAOHibernatePartida() {
		super(Partida.class);
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<Partida> buscarPartidasSinIniciar(int page, int entriesPerPage) {
		CriteriaBuilder cb = entityManager.getCriteriaBuilder();
		CriteriaQuery<Partida> criteriaQ = cb.createQuery(CLAZZ);
		Root<Partida> raiz = criteriaQ.from(CLAZZ);
		Predicate noIniciada = cb.isFalse(raiz.get("partidaIniciada"));
		criteriaQ.select(raiz).where(noIniciada);
		Query query = entityManager.createQuery(criteriaQ);
		query.setFirstResult((page -1) *entriesPerPage).setMaxResults(entriesPerPage);
		try {
			return query.getResultList();
		} catch(NoResultException e) {
			return new ArrayList<>(0);
		}
	}//buscarPartidasSinIniciar

	@Override
	public long cuentaPartidasSinIniciar() {
		CriteriaBuilder cb = entityManager.getCriteriaBuilder();
		CriteriaQuery<Long> queryContar = cb.createQuery(Long.class);
		Root<Partida> raiz = queryContar.from(CLAZZ);
		Predicate noIniciada = cb.isFalse(raiz.get("partidaIniciada"));
		queryContar.select(cb.count(raiz)).where(noIniciada);
		Query query = entityManager.createQuery(queryContar);
		try {
			return (Long) query.getSingleResult();
		} catch(NoResultException e) {
			return 0;
		}//intenta obtener un resultado
	}//cuentaPartidasSinIniciar

	@Override
	public Partida buscarPartidaSinIniciar(int id) {
		CriteriaBuilder cb = entityManager.getCriteriaBuilder();
		CriteriaQuery<Partida> criteriaQ = cb.createQuery(CLAZZ);
		Root<Partida> raiz = criteriaQ.from(CLAZZ);
		Predicate noIniciada = cb.isFalse(raiz.get("partidaIniciada"));
		Predicate noFinalizada = cb.isFalse(raiz.get("partidaFinalizada"));
		Predicate idIs = cb.equal(raiz.get("id"), id);
		criteriaQ.select(raiz).where(noIniciada, noFinalizada, idIs);
		Query query = entityManager.createQuery(criteriaQ);
		try {
			return (Partida) query.getSingleResult();
		} catch(NoResultException e) {
			return null;
		}
	}//buscarPartidaSinIniciar

	@Override
	public Partida buscarPartidaActivaSinFinalizar(int id) {
		CriteriaBuilder cb = entityManager.getCriteriaBuilder();
		CriteriaQuery<Partida> criteriaQ = cb.createQuery(CLAZZ);
		Root<Partida> raiz = criteriaQ.from(CLAZZ);
		Predicate iniciada = cb.isTrue(raiz.get("partidaIniciada"));
		Predicate noFinalizada = cb.isFalse(raiz.get("partidaFinalizada"));
		Predicate idIs = cb.equal(raiz.get("id"), id);
		criteriaQ.select(raiz).where(iniciada, noFinalizada, idIs);
		Query query = entityManager.createQuery(criteriaQ);
		try {
			return (Partida) query.getSingleResult();
		} catch(NoResultException e) {
			return null;
		}
	}

	@Override
	public List<Partida> buscarPartidasSinIniciarParaElJugador(List<Integer> partidas) {
		List<Partida> resultado = new ArrayList<>(partidas.size());
		for(Integer partida : partidas) {
			Partida part = buscarPartidaSinIniciar(partida);
			if(part == null) continue;
			resultado.add(part);
		}
		return resultado;
	}

}//DAO Hibernate Partida
