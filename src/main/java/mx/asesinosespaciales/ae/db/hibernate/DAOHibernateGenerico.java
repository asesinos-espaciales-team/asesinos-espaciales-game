package mx.asesinosespaciales.ae.db.hibernate;

import java.io.Serializable;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;

import org.springframework.dao.EmptyResultDataAccessException;

import mx.asesinosespaciales.ae.db.DAOGenerico;

/**
 * Implementaci&oacute;n general del DAOGenerico. No define ningun tipo de
 * entidad ni de sus llaves primarias; son abstraidos con gen&eacute;ricos.
 * Esta implementaci&oacute;n del DAOGenerico est&aacute; dise&ntilde;ada para
 * ser usada con <b>Hibernate</b>.
 * @author <a href="mailto:nachintoch@gmail.com" >Manuel "nachintoch" Castillo</a>
 * @param K - El tipo de la llave primaria de la entidad que maneja el DAO E.
 * @param E - El tipo de entidad que maneja el DAO.
 * @see <a href="https://hibernate.org/" >Hibernate</a>
 */
public abstract class DAOHibernateGenerico<K extends Serializable, E> implements DAOGenerico<K, E> {

	// atributos
	
	protected final Class<E> CLAZZ;
	
	@PersistenceContext
	protected EntityManager entityManager;
	
	// constructor
	
	protected DAOHibernateGenerico(Class<E> clazz) {
		CLAZZ = clazz;
	}
	
	// métodos
	
	@Override
	public void guardar(E entidad) {
		entityManager.persist(entidad);
		entityManager.flush();
	}
	
	@Override
	public E actualizar(E obj) {
		return entityManager.merge(obj);
	}
	
	@Override
	public void borrar(E obj) {
		entityManager.remove(obj);
	}
	
	@Override
	public void borrarPorLlave(K llave) {
		E entidad = buscar(llave);
		if(entidad == null) return;
		borrar(entidad);
	}
	
	@Override
	public E buscar(K llave) {
		try {
			return entityManager.find(CLAZZ, llave);
		} catch(EmptyResultDataAccessException | NoResultException e) {
			return null;
		}
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public List<E> obtenerTodos() {
		return entityManager.createQuery("from " +CLAZZ.getName()).getResultList();
	}
	
}
