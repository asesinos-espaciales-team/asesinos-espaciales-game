package mx.asesinosespaciales.ae.db;

import mx.asesinosespaciales.ae.modelos.Usuario;

public interface DAOUsuario extends DAOGenerico<String, Usuario> {

	void borrarPorUsername(String username);
	void borrarPorTokenSesion(String token);
	
	Usuario buscarPorUsername(String username);
	Usuario buscarPorTokenSesion(String token);
	
}
