package mx.asesinosespaciales.ae.db;

import java.io.Serializable;
import java.util.List;

/**
 * Interfaz que define los m&eacute;todos m&iacute;nimos para implementar un CRUD/ABD/ABM siguiendo el modelo de DAOs 
 * @author <a href="mailto:nachintoch@gmail.com" >Mannuel "Nachintoch" Castillo</a>
 * @param K - El tipo de la llave primaria de la Entidad que manoipula el DAO.
 * @param E - EL tipo de la entidad que manipula el DAO.
 */
public interface DAOGenerico<K extends Serializable, E> {

	/**
	 * Create - Crear.
	 * Altas de nuevos objetos en la base de datos.
	 * @param obj - El objeto a registrar en la base de datos.
	 */
	void guardar(E obj);
	
	/**
	 * Update - Actualiza.
	 * Cambios/Modificaciones al registro del objeto dado en base de datos.
	 * @param obj - El objeto a actualizar en base de datos.
	 * @return El objeto como qued&oacute; almacenado.
	 */
	E actualizar(E obj);
	
	/**
	 * Delete - Borrar.
	 * Bajas de objetos previamente registrados en base de datos.
	 * @param obj - El objeto a borrar de la base de datos.
	 */
	void borrar(E obj);
	
	/**
	 * Delete - Borrar.
	 * Bajas de objetos previamente registrados en la base de datos.
	 * @param llave - La llave del objeto a borrar de la base de datos.
	 */
	void borrarPorLlave(K llave);
	
	/**
	 * Busca el registro con la llave dada en la base de datos.
	 * @param llave - La llave del registro que se desea recuperar de la base de datos. 
	 * @return El objeto con la llave dada o null si no existe el registro.
	 */
	E buscar(K llave);
	
	/**
	 * Devuelve todos los registros en la colecci&oacute;n que representa la tabla.
	 * @return Lista con todos los objetos del tipo de Entidad E que maneje este DAO registrados en la BD. 
	 */
	List<E> obtenerTodos();
	
}//DAO Generico
