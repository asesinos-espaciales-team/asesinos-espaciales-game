package mx.asesinosespaciales.ae.db;

import java.util.List;

import mx.asesinosespaciales.ae.modelos.Partida;

public interface DAOPartida extends DAOGenerico<Integer, Partida> {

	/**
	 * Devuelve una lista con la p&aacute;gina solicitada de partidas sin iniciar.
	 * @param page - La p&aacute;gina deseada. Debe tener un valor positivo.
	 * @param entriesPerPage - El n&acute;mero de elementos por p&aacute;gina deseados
	 * @return List - Las partidas a&uacute;n no iniciadas que caben en la p&aacute;gina indicada.
	 */
	List<Partida> buscarPartidasSinIniciar(int page, int entriesPerPage);
	
	Partida buscarPartidaSinIniciar(int id);
	
	Partida buscarPartidaActivaSinFinalizar(int id);
	
	List<Partida> buscarPartidasSinIniciarParaElJugador(List<Integer> partidas);
	
	long cuentaPartidasSinIniciar();
	
}
