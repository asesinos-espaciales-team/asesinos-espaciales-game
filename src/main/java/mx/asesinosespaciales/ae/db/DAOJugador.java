package mx.asesinosespaciales.ae.db;

import java.util.List;

import mx.asesinosespaciales.ae.modelos.Jugador;
import mx.asesinosespaciales.ae.modelos.JugadorPK;

public interface DAOJugador extends DAOGenerico<JugadorPK, Jugador> {

	List<Jugador> buscarMejoresPuntuaciones(int limit);
	
	List<Integer> equiposEnPartida(int idPartida);
	
	List<Integer> idPartidasNoGanadasConElUsuario(String username);
	
	List<Jugador> buscarJugadoresEnPartida(int idPartida);
	
}
