package mx.asesinosespaciales.ae.controladores;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;

import mx.asesinosespaciales.ae.AsesinosEspaciales;
import mx.asesinosespaciales.ae.db.DAOJugador;
import mx.asesinosespaciales.ae.db.DAOPartida;
import mx.asesinosespaciales.ae.db.DAOUsuario;
import mx.asesinosespaciales.ae.engine.Arbitro;
import mx.asesinosespaciales.ae.modelos.EventoLobby;
import mx.asesinosespaciales.ae.modelos.EventoPartida;
import mx.asesinosespaciales.ae.modelos.Jugador;
import mx.asesinosespaciales.ae.modelos.JugadorPK;
import mx.asesinosespaciales.ae.modelos.Partida;
import mx.asesinosespaciales.ae.modelos.Usuario;
import mx.asesinosespaciales.ae.util.VectorND;

@Controller
public class ControladorWS {
	
	@Autowired
	private DAOUsuario daoUsuario;
	
	@Autowired
	private DAOJugador daoJugador;
	
	@Autowired
	private DAOPartida daoPartida;
	
	@Transactional
	@MessageMapping("/lobbyAction")
	@SendTo(ConfiguracionWebSocket.RUTA_BROKER_MENSAJERIA +"/evento/lobby")
	public EventoLobby eventoLobby(EventoLobby accionUsuario) {
		Usuario usr = validarRemitente(accionUsuario.getTokenSesion());
		Partida partida;
		Jugador jugador;
		try {
			partida = validarPartidaLobby(accionUsuario.getRoomid(), daoPartida);
		} catch(IllegalStateException e) {
			List<Integer> equiposDelUsuario = daoJugador.idPartidasNoGanadasConElUsuario(usr.getAlias());
			List<Partida> partidas = daoPartida.buscarPartidasSinIniciarParaElJugador(equiposDelUsuario);
			if(partidas.isEmpty()) return null;
			partida = partidas.get(0);
		}
		switch(accionUsuario.getTipoEvento()) {
		case EventoLobby.TIPO_INICIAR_PARTIDA :
			partida.setPartidaIniciada(true);
			daoPartida.actualizar(partida);
			iniciairArbitro(partida.getId(), daoJugador.buscarJugadoresEnPartida(partida.getId()));
			break;
		case EventoLobby.TIPO_USUARIO_CONECTADO :
			JugadorPK llaveJugador = new JugadorPK(usr, accionUsuario.getRoomid());
			if(daoJugador.buscar(llaveJugador) == null) {
				jugador = new Jugador();
				jugador.setEquipoN(partida.getJugadores().size() +1);
				jugador.setId(llaveJugador);
				jugador.setPartida(partida);
				daoJugador.guardar(jugador);
			}
			accionUsuario.setEquipos(daoJugador.equiposEnPartida(accionUsuario.getRoomid()).toArray(new Integer[] {}));
			break;
		case EventoLobby.TIPO_BORRAR_PARTIDA :
			daoPartida.borrar(partida);
			break;
		case EventoLobby.TIPO_USUARIO_DESCONECTADO :
			jugador = daoJugador.buscar(new JugadorPK(usr, accionUsuario.getRoomid()));
			if(jugador != null) {
				daoJugador.borrar(jugador);
			}
			break;
		case EventoLobby.TIPO_USUARIO_LISTO :
			break;
		case EventoLobby.TIPO_USUARIO_NOLISTO :
			break;
		case EventoLobby.TIPO_CAMBIAR_EQUIPO :
			jugador = daoJugador.buscar(new JugadorPK(usr, accionUsuario.getRoomid()));
			if(jugador == null) return null;
			jugador.setEquipoN(accionUsuario.getEquipos()[0]);
			daoJugador.actualizar(jugador);
			break;
		default :
			ignorarUsuario();
			throw new IllegalArgumentException("El EventoLobby conten\u00A1a una acci\00F3n no v\u00E1lida");
		}//registra las acciones de los usuarios
		// propaga el mensaje a todos
		return accionUsuario;
	}//eventoLobby
	
	@MessageMapping("/partidaAction")
	public void eventoAccionJugador(EventoPartida eventoJugador) {
		Usuario usr = validarRemitente(eventoJugador.getTokenSesion());
		validarPartidaActiva(eventoJugador.getRoomid(), daoPartida);
		Arbitro arbitro = AsesinosEspaciales.PARTIDAS_ACTIVAS.get(eventoJugador.getRoomid());
		switch (eventoJugador.getTipoEvento()) {
		case EventoPartida.TIPO_ACCION_ACELERACION :
			arbitro.acelerarJugador(usr.getAlias(), eventoJugador.decodificaAceleracion(), false);
			break;
		case EventoPartida.TIPO_ACCION_ACELERACION_CERO :
			arbitro.acelerarJugador(usr.getAlias(), new VectorND(2), false);
			break;
		case EventoPartida.TIPO_ACCION_DISPARO :
			arbitro.registrarDisparo(usr.getAlias(), false);
			break;
		case EventoPartida.TIPO_ACCION_ACELERACION_SOSTENIDA :
			arbitro.acelerarJugador(usr.getAlias(), eventoJugador.decodificaAceleracion(), true);
			break;
		case EventoPartida.TIPO_ACCION_ALTO_AL_FUEGO :
			arbitro.altoAlFuego(usr.getAlias());
			break;
		case EventoPartida.TIPO_ACCION_DISPARO_RAPIDO :
			arbitro.registrarDisparo(usr.getAlias(), true);
			break;
		default:
			ignorarUsuario();
			throw new IllegalArgumentException("El EventoPartida conten\u00A1a una acci\00F3n no v\u00E1lida");
		}
	}//eventoAccionJugador

	private void iniciairArbitro(int partidaId, List<Jugador> jugadores) {
		Arbitro arbitro = new Arbitro(partidaId, jugadores);
		AsesinosEspaciales.PARTIDAS_ACTIVAS.put(partidaId, arbitro);
		AsesinosEspaciales.FABRICA_HILOS.submit(arbitro);
	}//iniciarArbitro
	
	private Usuario validarRemitente(String token) {
		Usuario usr = SesionUtils.validarToken(daoUsuario, token, null);
		if(usr == null) {
			// tirar una excepción en un controlador no termina el servidor
			ignorarUsuario();
			throw new IllegalArgumentException("Se ha intentado una conexi\u00F3n con un token inv\u00E1lido");
		}//verifica que exista un usuario con una sesión válida con el token dado
		return usr;
	}//validarRemitente
	
	/**
	 * Valida si la partida a la que hace referencia el mensaje de WebSocket recibido es v&aacute;lida. Puesto que
	 * en el Lobby una partida es válida &uacute;nicamente cuando no ha sido iniciada; se verifica no solo que la
	 * partida exista, si no que no haya sido iniciada tambi&eacute;n.
	 * @param roomId - El room del WebSocket corresponde con el identificador de la partida que se prepara en cada
	 * Lobby.
	 * @param daoPartida - Referencia a un DAO Partida para validar la partida en la base de datos.
	 * @return Partida - Si la partida con id == roomId que no ha sido iniciada.
	 * @throws IllegalArgumentException - Si la partida con id == roomId no existe o ha iniciado.
	 */
	public static Partida validarPartidaLobby(int roomId, DAOPartida daoPartida) throws IllegalArgumentException {
		Partida partida = daoPartida.buscarPartidaSinIniciar(roomId);
		if(partida == null) {
			// tirar una excepción en un controlador no termina el servidor
			throw new IllegalArgumentException("Se ha recibido una partida inv\u00E1lida");
		}//verifica que la partida exista y no haya sido iniciada
		return partida;
	}//validarPartida
	
	public static Partida validarPartidaActiva(int partidaId, DAOPartida daoPartida) throws IllegalArgumentException {
		Partida partida = daoPartida.buscarPartidaActivaSinFinalizar(partidaId);
		if(partida == null) {
			// tirar una excepción en un controlador no termina el servidor
			throw new IllegalArgumentException("Se ha recibido una partida inv\u00E1lida");
		}//verifica que la partida exista y no haya sido iniciada
		return partida;
	}
	
	private void ignorarUsuario() {
		// TODO cerrar una conexión de web sockets que está enviando basura
	}
	
}//Controlador WS
