package mx.asesinosespaciales.ae.controladores;

import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import mx.asesinosespaciales.ae.db.DAOJugador;
import mx.asesinosespaciales.ae.db.DAOPartida;
import mx.asesinosespaciales.ae.db.DAOUsuario;
import mx.asesinosespaciales.ae.engine.Arbitro;
import mx.asesinosespaciales.ae.modelos.Jugador;
import mx.asesinosespaciales.ae.modelos.JugadorPK;
import mx.asesinosespaciales.ae.modelos.Partida;
import mx.asesinosespaciales.ae.modelos.Usuario;

/*
 * el  controladorWeb se encarga de hacer la conexion a las vistas menu, puntuacion,
 * lobby, partida, game. Para entablar la conexin se pasa como parametro el cookie 
 * el cual es validado para ver si la sesion del usuario no ha expirado.
 * */
@Controller
public class ControladorWeb {

	@Autowired
	private DAOPartida daoPartida;
	
	@Autowired
	private DAOUsuario daoUsuario;
	
	@Autowired
	private DAOJugador daoJugador;
	
	public static final byte MAX_LONG_LISTA_PARTIDAS = 20;
	
	public static final byte MAX_MEJORES_PUNTUACIONES = 30;
	
	@Transactional
	@GetMapping(value= {"/index","/"})
	public String mainMenu(@CookieValue(name=SesionUtils.COOKIE_SESION, required=false) String tokenSesion,
			Model model, HttpServletResponse res) {
		if(SesionUtils.validarToken(daoUsuario, tokenSesion, res) != null)
			model.addAttribute("tokenSesion", tokenSesion);
		return "menu";
	}

	
	@GetMapping("/mejoresPuntuaciones")
	public String  mejoresPuntuaciones(@CookieValue(name=SesionUtils.COOKIE_SESION, required=false) String tokenSesion,
			Model model) {
		model.addAttribute("tokenSesion", tokenSesion);
		List<Jugador> losMasVagos = daoJugador.buscarMejoresPuntuaciones(MAX_MEJORES_PUNTUACIONES);
		model.addAttribute("mejoresJugadores", losMasVagos);
		return "puntuacion";
	}

	@Transactional
	@GetMapping("/lobby")
	public String  lobbyP(@CookieValue(name=SesionUtils.COOKIE_SESION, required=true) String tokenSesion,
			Model model, HttpServletResponse res) {
		Usuario usr = daoUsuario.buscarPorTokenSesion(tokenSesion);
		if(!SesionUtils.validarUsuario(daoUsuario, usr, res)) return "error";
		List<Integer> partidasNoGanadas = daoJugador.idPartidasNoGanadasConElUsuario(usr.getAlias());
		Partida partida = null;
		for(Integer pId : partidasNoGanadas) {
			partida = daoPartida.buscarPartidaSinIniciar(pId);
			if(partida != null) break;
		}
		if(partida == null){
			partida = new Partida();
			partida.setOrganizador(usr);
			daoPartida.guardar(partida);
			Jugador jugador = new Jugador();
			jugador.setId(new JugadorPK(usr, partida.getId()));
			jugador.setPartida(partida);
			daoJugador.guardar(jugador);
		}
		List<Jugador> jugadoresEnPartida = daoJugador.buscarJugadoresEnPartida(partida.getId());
		model.addAttribute("jugadores", jugadoresEnPartida);
		model.addAttribute("idPartida", partida.getId());
		model.addAttribute("tokenSesion", tokenSesion);
		List<Integer> equiposPartida = daoJugador.equiposEnPartida(partida.getId());
		model.addAttribute("equiposPartida", equiposPartida.size());
		model.addAttribute("username", partida.getOrganizador().getAlias());
		return "lobby";
	}
	
	@GetMapping("/verPartida")
	public String  verPartida(@CookieValue(name=SesionUtils.COOKIE_SESION, required=false) String tokenSesion,
			@RequestParam(name="page", required=false) Integer page, Model model, HttpServletResponse res) {
		if(page == null || page < 1) page = 1;
		List<Partida> partidas = daoPartida.buscarPartidasSinIniciar(page, MAX_LONG_LISTA_PARTIDAS);
		model.addAttribute("partidas", partidas);
		model.addAttribute("pagina", page);
		double totalPartidasEnEspera = daoPartida.cuentaPartidasSinIniciar();
		model.addAttribute("totalPaginas", (long) Math.ceil(totalPartidasEnEspera /MAX_LONG_LISTA_PARTIDAS));
		Usuario usr = SesionUtils.validarToken(daoUsuario, tokenSesion, res);
		if(usr == null) return "partida";
		model.addAttribute("tokenSesion", tokenSesion);
		model.addAttribute("username", usr.getAlias());
		List<Integer> equiposDelUsuario = daoJugador.idPartidasNoGanadasConElUsuario(usr.getAlias());
		List<Partida> partidasPendientesParaJugador = daoPartida.buscarPartidasSinIniciarParaElJugador(equiposDelUsuario);
		if(partidasPendientesParaJugador.isEmpty()) return "partida";
		Partida partida = partidasPendientesParaJugador.get(0);
		model.addAttribute("partidaEnCurso", partida);
		List<Integer> equiposPartida = daoJugador.equiposEnPartida(partidasPendientesParaJugador.get(0).getId());
		model.addAttribute("equiposPartida", equiposPartida.size());
		return "partida";
	}
	
	@GetMapping("/jugar")
	public String jugar(@CookieValue(name=SesionUtils.COOKIE_SESION, required=false) String tokenSesion,
			@RequestParam(name="idPartida", required=true) int idPartida, Model model, HttpServletResponse res) {
		Usuario usr;
		if((usr = SesionUtils.validarToken(daoUsuario, tokenSesion, res)) != null) {
			model.addAttribute("tokenSesion", tokenSesion);
			model.addAttribute("username", usr.getAlias());
		}
		try {
			ControladorWS.validarPartidaActiva(idPartida, daoPartida);
			model.addAttribute("idPartida", idPartida);
		} catch(IllegalArgumentException e) {
			model.addAttribute("idPartida", -1);
		}
		model.addAttribute("dimensionesEspacio", Arbitro.DIMENSIONES_MUNDO);
		return "game";
	}
	
}//Controlador Web
