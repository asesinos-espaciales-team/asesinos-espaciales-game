package mx.asesinosespaciales.ae.controladores;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;

import mx.asesinosespaciales.ae.db.DAOUsuario;
import mx.asesinosespaciales.ae.modelos.Usuario;

public final class SesionUtils {

	public static final String COOKIE_SESION = "mx.asesinosespaciales.ae.cookies.sesion";
	
	public static final int TOKEN_SESION_TTL = 1000 *60 *60 *24 *7;
	
	private SesionUtils() {
		// this is not the method you are looking for
	}
	
	public static boolean validateSesion(Usuario usr) {
		return StringUtils.isNotEmpty(usr.getToken()) &&
				System.currentTimeMillis() <= usr.getFecha().getTime() +TOKEN_SESION_TTL;
	}
	
	public static synchronized String generateTokenSesion(DAOUsuario daoUsuario) {
		String token;
		do {
			token = RandomStringUtils.random(255, true, true);
		} while(daoUsuario.buscarPorTokenSesion(token) != null);
		return token;
	}//generateTokenSesion
	
	public static boolean setTokenSesion(HttpServletResponse res, String token) {
		Cookie cookie = new Cookie(COOKIE_SESION, token);
		cookie.setHttpOnly(true);
		cookie.setMaxAge(TOKEN_SESION_TTL /1000);
		cookie.setPath("/");
		res.addCookie(cookie);
		return true;
	}//setTokenSesion
	
	public static boolean removeTokenSesion(HttpServletResponse res) {
		Cookie cookie = new Cookie(COOKIE_SESION, null);
		cookie.setHttpOnly(true);
		cookie.setMaxAge(0);
		cookie.setPath("/");
		res.addCookie(cookie);
		return true;
	}
	
	/**
	 * Valida si el token dado corresponde con alg&uacute;n usuario cuya
	 * sesi&oacute;n no haya expirado.
	 * @param daoUsuario - Referencia a un DAO de usuario
	 * @param tokenSesion - El token de un usiario.
	 * @param res - La respuesta del servidor al cliente que env&iacute;o el
	 * token.
	 * @return Usuario - Null si el token es inv&aacute;lido, false en otro
	 * caso.
	 */
	public static Usuario validarToken(DAOUsuario daoUsuario, String tokenSesion, HttpServletResponse res) {
		if(StringUtils.isEmpty(tokenSesion)) return null;
		Usuario usr = daoUsuario.buscarPorTokenSesion(tokenSesion);
		if(!validarUsuario(daoUsuario, usr, res)) return null;
		return usr;
	}//validarToken
	
	public static boolean validarUsuario(DAOUsuario daoUsuario, Usuario usr, HttpServletResponse res) {
		if(usr == null) {
			res.setStatus(HttpServletResponse.SC_GONE);
			SesionUtils.removeTokenSesion(res);
			return false;
		}
		if(!SesionUtils.validateSesion(usr)) {
			SesionUtils.removeTokenSesion(res);
			usr.setToken(null);
			daoUsuario.actualizar(usr);
			res.setStatus(HttpServletResponse.SC_GONE);
			return false;
		}
		return true;
	}//validarUsuario
	
}
