package mx.asesinosespaciales.ae.controladores;

import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import mx.asesinosespaciales.ae.db.DAOUsuario;
import mx.asesinosespaciales.ae.modelos.Usuario;


@RestController
public class ControladorRest {

	Pattern pp = Pattern.compile("^[a-zA-Z0-9_!#$%&’*+/=?`{|}~^.-]+@[a-zA-Z0-9]+[[.][a-zA-Z0-9]+]+$");	
	@Autowired
	private DAOUsuario daoUsuario;
	
	@Transactional
	@PostMapping("/login")
	public boolean login(@RequestParam(name="email", required=true) String email,
			 @RequestParam(name="pwd", required=true) String password, HttpServletResponse res) {
		if(StringUtils.isAnyEmpty(email, password)) {
			res.setStatus(HttpServletResponse.SC_NOT_ACCEPTABLE);
			return false;
		}
		Usuario usr = daoUsuario.buscar(email);
		if(usr == null) {
			res.setStatus(HttpServletResponse.SC_NOT_FOUND);
			return false;
		}
		if(!usr.getContras().equals(password)) {
			res.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
			return false;
		}
		String token;
		usr.setFecha(new Date());
		token = SesionUtils.generateTokenSesion(daoUsuario);
		usr.setToken(token);
		daoUsuario.actualizar(usr);
		return SesionUtils.setTokenSesion(res, token);
	}
	
	@Transactional
	@PostMapping("/logout")
	public boolean logout(@CookieValue(name=SesionUtils.COOKIE_SESION, required=false) String tokenSesion,
			HttpServletResponse res) {
		if(StringUtils.isEmpty(tokenSesion)) {
			res.setStatus(HttpServletResponse.SC_NOT_ACCEPTABLE);
			return false;
		}
		Usuario usr = daoUsuario.buscarPorTokenSesion(tokenSesion);
		if(usr == null) {
			res.setStatus(HttpServletResponse.SC_NOT_FOUND);
			return true;
		}
		usr.setToken(null);
		daoUsuario.actualizar(usr);
		return SesionUtils.removeTokenSesion(res);
	}
	
	@Transactional
	@PostMapping("/singup")
	public boolean singup(@RequestParam(name="email", required=true) String email,
			@RequestParam(name="username", required=true) String username,
			@RequestParam(name="pwd", required=true) String password, HttpServletResponse res) {
		if(StringUtils.isAnyEmpty(email, username, password)) {
			res.setStatus(HttpServletResponse.SC_NOT_ACCEPTABLE);
			return false;
		}
		Matcher mm = pp.matcher(email);
		//valida que el email tenga la estructura adecuada y la longitud de las cadenas proporcionadas
	    if (mm.matches() && email.length()<=1024 && username.length()<=128) {
	    	if(daoUsuario.buscar(email) != null || daoUsuario.buscarPorUsername(username) != null) {
	    		res.setStatus(HttpServletResponse.SC_CONFLICT);
	  			return false;
	    	}
	    	String token = SesionUtils.generateTokenSesion(daoUsuario);
	    	Usuario usr = new Usuario(email, username, token, password, new Date());
	    	daoUsuario.guardar(usr);
	    	return SesionUtils.setTokenSesion(res, token);
	    } else { //en caso contrario regresa false
	    	res.setStatus(HttpServletResponse.SC_NOT_ACCEPTABLE);
	    	return false;
	    }
	}

}//Controlador REST
