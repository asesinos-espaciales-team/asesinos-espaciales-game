package mx.asesinosespaciales.ae.controladores;

import org.springframework.context.annotation.Configuration;
import org.springframework.messaging.simp.config.MessageBrokerRegistry;
import org.springframework.web.socket.config.annotation.EnableWebSocketMessageBroker;
import org.springframework.web.socket.config.annotation.StompEndpointRegistry;
import org.springframework.web.socket.config.annotation.WebSocketMessageBrokerConfigurer;

@Configuration
@EnableWebSocketMessageBroker
public class ConfiguracionWebSocket implements WebSocketMessageBrokerConfigurer {

	public static final String RUTA_BROKER_MENSAJERIA = "/ascomm";
	
	@Override
	public void configureMessageBroker(MessageBrokerRegistry registry) {
		registry.enableSimpleBroker(RUTA_BROKER_MENSAJERIA);
		registry.setApplicationDestinationPrefixes("/app");
	}//configureMessageBroker
	
	@Override
	public void registerStompEndpoints(StompEndpointRegistry registry) {
		registry.addEndpoint("/aews").setAllowedOrigins("*").withSockJS();
	}//registerStompEndpoints
	
}//WebSocketConfig

