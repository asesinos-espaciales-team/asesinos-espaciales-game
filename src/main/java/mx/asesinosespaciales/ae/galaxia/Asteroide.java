package mx.asesinosespaciales.ae.galaxia;

import mx.asesinosespaciales.ae.modelos.Jugador;
import mx.asesinosespaciales.ae.util.Vector;
import mx.asesinosespaciales.ae.util.VectorND;

/**
 * La clase Asteroide es una clase derivada de ObjetoDelEspacioProfundo.
 */
public class Asteroide extends ObjetoDelEspacioProfundo{

	// constructores

	public Asteroide(int equipo,Jugador propietario, float x, float y,float velocidadMaxima) {
        super(equipo,propietario,x,y,velocidadMaxima);
	}

	// métodos

	@Override	
	public void calcularSiguientePosicion(long dt) {
		//El método tiene sentido para asteroides en movimiento
		//(velocidad diferente de cero)
		Vector<Float> dX = new VectorND(0.0f,0.0f);
		this.validarVelocidad();
		dX.suma(this.velocidad);
		Float newNorma = new Float(dt*this.velocidad.norma()/1000.0f);
		dX.prodEscalar(newNorma);
		this.posicionInstantanea.suma(dX);
	}


	@Override
	protected boolean validarVelocidad() {
		if(this.velocidad.norma() > this.VELOCIDAD_MAXIMA) {
			Float newNorma = new Float(this.VELOCIDAD_MAXIMA/this.velocidad.norma());
			this.velocidad.prodEscalar(newNorma);
		}
		return true;
	}


}//Clase Asteroide
