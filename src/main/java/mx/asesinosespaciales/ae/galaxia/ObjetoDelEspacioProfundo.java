package mx.asesinosespaciales.ae.galaxia;

import mx.asesinosespaciales.ae.util.Vector;
import mx.asesinosespaciales.ae.util.VectorND;
import mx.asesinosespaciales.ae.modelos.Jugador;

/**
 * Los objetos del espacio profundo son instancias de clases que derivan
 * de la clase abstracta ObjetoDelEspacioProfundo
 */
public abstract class ObjetoDelEspacioProfundo {

	// atributos

	protected Vector<Float> velocidad;
	protected Vector<Float> posicionInstantanea;

	public final int EQUIPO;
	public final Jugador PROPIETARIO;
	protected final Vector<Float> POSICION_INICIAL;
	public final Vector<Float> DIMENSIONES;
	protected final float VELOCIDAD_MAXIMA;
	
	// constructores

	/**
	 * Constructor de la clase abstracta, este constructor fija los
	 * par&aacute;metros que son constantes para los objetos y requiere la
	 * memoria para los vectores  que son mutables.
	 * El objeto parte del reposo y su posici&oacute;n instant&aacute;nea es la inicial
	 * @param equipo - Identificador del EQUIPO.
	 * @param propietario - Identificador del PROPIETARIO.
	 * @param X - Primer elemento del vector POSICION_INICIAL.
	 * @param Y - Segundo elemento del vector POSICION_INICIAL.
	 * @param velocidadMaxima - VELOCIDAD_MAXIMA.
	 */
	public ObjetoDelEspacioProfundo(int equipo, Jugador propietario, float x, float y, float velocidadMaxima) {
		this.velocidad = new VectorND(0.0f,0.0f);
		this.posicionInstantanea = new VectorND(0.0f,0.0f);

		this.EQUIPO = equipo;
		this.PROPIETARIO = propietario;
		this.POSICION_INICIAL = new VectorND(x, y);
		this.DIMENSIONES = new VectorND(2);
		this.VELOCIDAD_MAXIMA = velocidadMaxima;

		this.posicionInstantanea.suma(this.POSICION_INICIAL);
	}

	// métodos
	
	/**
	 * Cambia la posición del objeto
	 * @param dt - Incremento de tiempo.
	 */
	public abstract void calcularSiguientePosicion(long dt);


	/**
	 * Realiza una comprobaci&oacute;n de la velocidad.
	 * @return Valor de la prueba condicional.
	 */
	 protected abstract boolean validarVelocidad();
	 
	 public Vector<Float> getPosicionInstantanea() {
		 return new VectorND(this.posicionInstantanea.getElement(0),
				 this.posicionInstantanea.getElement(0));
	 }//compruebaColision
	 
	 public boolean compruebaColision(ObjetoDelEspacioProfundo cuerpo) {
		 if(cuerpo == null) return false;
		 float x = posicionInstantanea.getElement(0);
		 float y = posicionInstantanea.getElement(1);
		 float ancho = DIMENSIONES.getElement(0);
		 float alto = DIMENSIONES.getElement(1);
		 float xCuerpo = cuerpo.getPosicionInstantanea().getElement(0);
		 float yCuerpo = cuerpo.getPosicionInstantanea().getElement(1);
		 float anchoCuerpo = cuerpo.DIMENSIONES.getElement(0);
		 float altoCuerpo = cuerpo.DIMENSIONES.getElement(1);
		 return xCuerpo < x +ancho && x < xCuerpo +anchoCuerpo && yCuerpo < y +alto && x < yCuerpo +altoCuerpo;
	 }//compruebaColision

	public Vector<Float> getVelocidad() {
		return velocidad;
	}

}//Clase ObjetoDelEspacioProfundo



