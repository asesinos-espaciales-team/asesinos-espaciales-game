package mx.asesinosespaciales.ae.galaxia;

import mx.asesinosespaciales.ae.modelos.Jugador;
import mx.asesinosespaciales.ae.util.Vector;
import mx.asesinosespaciales.ae.util.VectorND;

/**
 * La clase Municion es una clase derivada de ObjetoDelEspacioProfundo.
 */
public class Municion extends ObjetoDelEspacioProfundo{

	// atributos

	public float ALCANCE_MAXIMO;

	// constructores

	public Municion(int equipo,Jugador propietario,float x,float y,float velocidadMaxima) {
        super(equipo,propietario,x,y,velocidadMaxima);
	}

	// métodos

	@Override	
	public void calcularSiguientePosicion(long dt) {
		Vector<Float> dX = new VectorND(0.0f,0.0f);
		this.validarVelocidad();
		dX.suma(this.velocidad);
		Float newNorma = new Float(dt*this.velocidad.norma()/1000.0f);
		dX.prodEscalar(newNorma);
		this.posicionInstantanea.suma(dX);
	}


	@Override
	protected boolean validarVelocidad() {
		if(this.velocidad.norma() > this.VELOCIDAD_MAXIMA) {
			Float newNorma = new Float(this.VELOCIDAD_MAXIMA/this.velocidad.norma());
			this.velocidad.prodEscalar(newNorma);
		}
		return true;
	}

	/**
	 * Verifica que la munici&oacute;n siga existiendo en el juego.
	 * @return Valor de la prueba condicional.
	 */
	 private boolean estaFueraDeAlcance() {
		 Vector<Float> dX = new VectorND(0.0f,0.0f);
		 dX.suma(this.POSICION_INICIAL);
		 dX.resta(this.posicionInstantanea);
		 return dX.norma() > this.ALCANCE_MAXIMO;
	 }


	 /**
	  * Cambia la posici&oacute;n de la munici&oacute;n.
	  * Adem&aacute;s compruba que la munici&oacute;n siga existiendo en el mundo del juego.
	  * @param dt - Incremento de tiempo.
	  */
	  public void moverse(long dt) {
		  //Listo pata integrar
		  this.calcularSiguientePosicion(dt);
		  if(this.estaFueraDeAlcance()) {
			  this.posicionInstantanea.incrementar(0, Float.NaN);
			  this.posicionInstantanea.incrementar(1, Float.NaN);			  
		  }
		  //De algún modo la munición desaparece
	  }

}//Clase Municion
