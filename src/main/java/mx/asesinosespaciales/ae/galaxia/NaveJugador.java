package mx.asesinosespaciales.ae.galaxia;

import mx.asesinosespaciales.ae.modelos.Jugador;
import mx.asesinosespaciales.ae.util.Vector;
import mx.asesinosespaciales.ae.util.VectorND;

/**
 * La clase NaveJugador es una clase derivada de ObjetoDelEspacioProfundo.
 */
public class NaveJugador extends ObjetoDelEspacioProfundo{
	
	// atributos

	public static final byte ENERGIA_MAXIMA = 10;
	public static final byte MAXIMO_TIEMPO_ESPERA = 30;
	private byte energia;
	
	public static final byte ACELERACION_MOTORES = 5;
	
	// constructores

	public NaveJugador(int equipo,Jugador propietario,float x,float y,float velocidadMaxima) {
        super(equipo,propietario,x,y,velocidadMaxima);
	}

	// métodos

	@Override	
	public void calcularSiguientePosicion(long dt) {
		Vector<Float> dX = new VectorND(0.0f,0.0f);
		this.validarVelocidad();
		dX.suma(this.velocidad);
		Float newNorma = new Float(dt*this.velocidad.norma()/1000.0f);
		dX.prodEscalar(newNorma);
		this.posicionInstantanea.suma(dX);
	}


	@Override
	protected boolean validarVelocidad() {
		if(this.velocidad.norma() > this.VELOCIDAD_MAXIMA) {
			Float newNorma = new Float(this.VELOCIDAD_MAXIMA/this.velocidad.norma());
			this.velocidad.prodEscalar(newNorma);
		}
		return true;
	}

	/**
	 * Cambia la velocidad  de acuerdo al valor de aceleraci&oacute;n.
	 * @param aceleracion - Vector que modificarm&aacute; la velocidad actual.
	 * @param dt - Incremento de tiempo.
	 */
	 public void acelerar(Vector<Float> aceleracion,long dt) {
			Vector<Float> dV = new VectorND(0.0f,0.0f);
			dV.suma(aceleracion);
			float factEscala = dt/1000.0f;
			dV.prodEscalar(factEscala);
			this.velocidad.suma(dV);
			this.validarVelocidad();	
	 }

	 /**
	  * Permite acelerar y disparar.
	  */
	  public void registrarAccion() {
		  //Listo para integrar
	  }

	  /**
	   * Verifica el nivel de energ&iacute;a de la nave.
	   * Si est&aacute; en cero est&aacute; descalificado, adem&aacute;s no puede ser superior al m&aacute;ximo.
	   * @return Valor de la prueba condicional.
	   */
	   public boolean estaDescalificado() {
		   return energia <= 0;		   
	   }
	   
	   public void impactar(byte damage) {
		   energia -= damage;
	   }

	   public void reparar(byte energia) {
		   this.energia += energia;
		   if(this.energia > NaveJugador.ENERGIA_MAXIMA) this.energia = NaveJugador.ENERGIA_MAXIMA; 
	   }

	public byte getEnergia() {
		return energia;
	}

}//Clase NaveJugador
