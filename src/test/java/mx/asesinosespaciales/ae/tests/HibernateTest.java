package mx.asesinosespaciales.ae.tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.Date;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import mx.asesinosespaciales.ae.db.DAOJugador;
import mx.asesinosespaciales.ae.db.DAOPartida;
import mx.asesinosespaciales.ae.db.DAOUsuario;
import mx.asesinosespaciales.ae.modelos.Jugador;
import mx.asesinosespaciales.ae.modelos.JugadorPK;
import mx.asesinosespaciales.ae.modelos.Partida;
import mx.asesinosespaciales.ae.modelos.Usuario;

@RunWith(SpringRunner.class)
@DataJpaTest
@AutoConfigureTestDatabase(replace = Replace.NONE)
public class HibernateTest {
	
	@Autowired
	private DAOUsuario daoUsuario;
	
	@Autowired
	private DAOPartida daoPartida;
	
	@Autowired
	private DAOJugador daoJugador;
	
	public static final String TEST_USERNAME = "rockstar89";
	public static final String CORREO_PRUEBA = "correo@no.validado";
	public static final String TEST_PASSWORD = "password123";
	
	private Integer popullateDatabase() {
		Usuario usr = new Usuario(CORREO_PRUEBA, TEST_USERNAME, null, "textoplano", null);
		daoUsuario.guardar(usr);
		assertEquals(CORREO_PRUEBA, usr.getCorreo());
		Partida partida = new Partida(null, usr, false, false);
		daoPartida.guardar(partida);
		assertNotNull(partida.getId());
		Jugador jugador = new Jugador(usr, partida, 0, 0, false);
		daoJugador.guardar(jugador);
		assertEquals(partida.getId().intValue(), jugador.getId().getIdPartida());
		assertEquals(TEST_USERNAME, jugador.getId().getUser().getAlias());
		return partida.getId();
	}
	
	@Test
	public void createTest() {
		popullateDatabase();
	}
	
	@Test
	public void updateTest() {
		Integer idPartidaCreada = popullateDatabase();
		Usuario usr = daoUsuario.buscarPorUsername(TEST_USERNAME);
		usr.setContras(TEST_PASSWORD);
		usr.setFecha(new Date());
		usr = daoUsuario.actualizar(usr);
		assertEquals(TEST_PASSWORD, usr.getContras());
		assertNotNull(usr.getFecha());
		Partida partida = daoPartida.buscar(idPartidaCreada);
		partida.setPartidaIniciada(true);
		partida.setPartidaFinalizada(true);
		partida = daoPartida.actualizar(partida);
		assertTrue(partida.getPartidaIniciada());
		assertTrue(partida.getPartidaFinalizada());
		final int puntuacion = 1000;
		Jugador jugador = daoJugador.buscar(new JugadorPK(usr, partida.getId()));
		jugador.setBanderaG(true);
		jugador.setPuntuacionN(puntuacion);
		jugador = daoJugador.actualizar(jugador);
		assertTrue(jugador.getBanderaG());
		assertEquals(puntuacion, jugador.getPuntuacionN());
	}
	
	@Test
	public void deleteTest() {
		Integer idPartidaCreada = popullateDatabase();
		Usuario usr = daoUsuario.buscarPorUsername(TEST_USERNAME);
		JugadorPK llaveJugador = new JugadorPK(usr, 1);
		daoJugador.borrarPorLlave(llaveJugador);
		assertNull(daoJugador.buscar(llaveJugador));
		daoUsuario.borrar(usr);
		assertNull(daoUsuario.buscarPorUsername(TEST_USERNAME));
		daoPartida.borrarPorLlave(idPartidaCreada);
		assertNull(daoPartida.buscar(idPartidaCreada));
	}
	
}
