package mx.asesinosespaciales.ae.tests;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import mx.asesinosespaciales.ae.controladores.ControladorRest;
import mx.asesinosespaciales.ae.controladores.ControladorWeb;

@RunWith(SpringRunner.class)
@WebMvcTest(controllers = {ControladorWeb.class, ControladorRest.class})
public class ControllerTests {

	@Autowired
	private MockMvc mockMvc;
	
	@Test
	public void testControladorWeb() throws Exception {
		mockMvc.perform(MockMvcRequestBuilders.get("/")).andExpect(MockMvcResultMatchers.status().isOk());
		mockMvc.perform(MockMvcRequestBuilders.get("/index")).andExpect(MockMvcResultMatchers.status().isOk());
		mockMvc.perform(MockMvcRequestBuilders.get("/mejoresPuntuaciones"))
			.andExpect(MockMvcResultMatchers.status().isOk());
		mockMvc.perform(MockMvcRequestBuilders.get("/mejoresPuntuaciones"))
			.andExpect(MockMvcResultMatchers.status().isOk());
		mockMvc.perform(MockMvcRequestBuilders.get("/verPartida")).andExpect(MockMvcResultMatchers.status().isOk());
	}
	
}
